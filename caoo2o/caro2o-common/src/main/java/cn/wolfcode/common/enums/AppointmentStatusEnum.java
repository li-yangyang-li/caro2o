package cn.wolfcode.common.enums;

/**
 * 操作状态
 * 
 * @author ruoyi
 *
 */
public enum AppointmentStatusEnum
{
    //每个类型都是枚举类的对象
    APPOINTING ,
    ARRIVED ,
    USER_CANCEL ,
    TIMEOUT_CANCEL ,
    SETTLED ,
    PAID
}
