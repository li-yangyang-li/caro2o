package cn.wolfcode;

import cn.wolfcode.common.utils.StringUtils;
import com.jayway.jsonpath.internal.Utils;
import org.activiti.api.process.model.events.SequenceFlowEvent;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Collection;

/**
 * @author zslstart
 * @Date 2024-01-30 11:36
 */

@SpringBootTest
public class activitiTest {
    @Autowired
    private RepositoryService repositoryService;

    @Test
    public void getLimitDiscountAmount(){
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("car_package_audit")
                .processDefinitionVersion(9)
                .singleResult();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinition.getId());
        Process process = bpmnModel.getProcessById(processDefinition.getKey());
        Collection<FlowElement> flowElements = process.getFlowElements();
        for (FlowElement flowElement : flowElements) {
            if (flowElement instanceof SequenceFlow){
                //强制类型转换
                SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                String conditionExpression = sequenceFlow.getConditionExpression();
                if (StringUtils.isNotEmpty(conditionExpression) && conditionExpression.contains("discountAmount>")){
                    int i = conditionExpression.indexOf(">");
                    String substring = conditionExpression.substring(i+1, conditionExpression.length() - 1).trim();
                    System.out.println(substring);

                }
            }
        }

    }
}
