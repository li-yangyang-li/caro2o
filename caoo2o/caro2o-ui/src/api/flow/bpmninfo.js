import request from '@/utils/request'

// 查询流程定义明细列表
export function listBpmninfo(query) {
  return request({
    url: '/flow/bpmninfo/list',
    method: 'get',
    params: query
  })
}

// 查询流程定义明细详细
export function getBpmninfo(id) {
  return request({
    url: '/flow/bpmninfo/' + id,
    method: 'get'
  })
}

// 新增流程定义明细
export function addBpmninfo(data) {
  return request({
    url: '/flow/bpmninfo',
    method: 'post',
    data: data
  })
}

// 修改流程定义明细
export function updateBpmninfo(data) {
  return request({
    url: '/flow/bpmninfo',
    method: 'put',
    data: data
  })
}

// 撤销流程定义明细
export function delBpmninfo(id) {
  return request({
    url: '/flow/bpmninfo/' + id,
    method: 'delete'
  })
}

//查看流程文件
export function getResourceFile(id,type) {
  if (type === "img"){
    return request({
      url: '/flow/bpmninfo/resource/' + id+'/'+type,
      method: 'get',
      responseType: 'blob'
    })
  }else{
    return request({
      url: '/flow/bpmninfo/resource/' + id+'/'+type,
      method: 'get',
    })
  }

}



//axios文件上传接口
export function deploymentBpmn(data) {
  let formDate = new FormData()
  formDate.append('bpmnType',data.bpmnType)
  formDate.append('info',data.info)
  formDate.append('bpmnFile',data.bpmnFile)
  return request.post(
     '/flow/bpmninfo/deploymentBpmn' ,formDate,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
}
