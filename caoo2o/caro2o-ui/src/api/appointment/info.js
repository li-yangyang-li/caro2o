import request from '@/utils/request'

// 查询养修信息预约列表
export function listInfo(query) {
  return request({
    url: '/appointment/info/list',
    method: 'get',
    params: query
  })
}

// 查询养修信息预约详细
export function getInfo(id) {
  return request({
    url: '/appointment/info/' + id,
    method: 'get'
  })
}

// 新增养修信息预约
export function addInfo(data) {
  return request({
    url: '/appointment/info',
    method: 'post',
    data: data
  })
}

// 修改养修信息预约
export function updateInfo(data) {
  return request({
    url: '/appointment/info',
    method: 'put',
    data: data
  })
}

// 删除养修信息预约
export function delInfo(id) {
  return request({
    url: '/appointment/info/' + id,
    method: 'delete'
  })
}
//修改到店状态

export function patchStatus(id) {
  return request({
    url: '/appointment/info/' + id,
    method: 'patch'
  })
}
export function patchCancel(id) {
  return request({
    url: '/appointment/info/cancel/' + id,
    method: 'patch'
  })
}
