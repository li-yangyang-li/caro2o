import request from '@/utils/request'

// 查询服务项列表
export function listServiceitem(query) {
  return request({
    url: '/appointment/serviceitem/list',
    method: 'get',
    params: query
  })
}

// 查询服务项详细
export function getServiceitem(id) {
  return request({
    url: '/appointment/serviceitem/' + id,
    method: 'get'
  })
}

//查询审核人信息
export function getAuditInfo(id) {
  return request({
    url: '/appointment/serviceitem/auditInfo/'+id,
    method: 'get'
  })
}

// 新增服务项
export function addServiceitem(data) {
  return request({
    url: '/appointment/serviceitem',
    method: 'post',
    data: data
  })
}

// 修改服务项
export function updateServiceitem(data) {
  return request({
    url: '/appointment/serviceitem',
    method: 'put',
    data: data
  })
}
//上下架服务项
export function updateServiceSaleStatus(id) {
  return request({
    url: '/appointment/serviceitem/SaleStatus/' +id,
    method: 'patch',
  })
}

// 删除服务项
export function delServiceitem(id) {
  return request({
    url: '/appointment/serviceitem/' + id,
    method: 'delete'
  })
}
