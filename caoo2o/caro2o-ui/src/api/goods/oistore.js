import request from '@/utils/request'

// 查询出入库单据列表
export function listOistore(query) {
  return request({
    url: '/goods/oistore/list',
    method: 'get',
    params: query
  })
}

// 查询出入库单据详细
export function getOistore(id) {
  return request({
    url: '/goods/oistore/' + id,
    method: 'get'
  })
}

// 新增出入库单据
export function addOistore(data) {
  return request({
    url: '/goods/oistore',
    method: 'post',
    data: data
  })
}
export function getBillDetail(id) {
  return request({
    url: '/goods/oistore/getBillDetail/'+id,
    method: 'get',

  })
}

// 修改出入库单据
export function updateOistore(data) {
  return request({
    url: '/goods/oistore',
    method: 'put',
    data: data
  })
}

// 删除出入库单据
export function delOistore(id) {
  return request({
    url: '/goods/oistore/' + id,
    method: 'delete'
  })
}


// 查询出入库单据明细列表
export function listStockitem(query) {
  return request({
    url: '/goods/stockitem/list',
    method: 'get',
    params: query
  })
}

// 查询出入库单据明细详细
export function getStockitem(id) {
  return request({
    url: '/goods/stockitem/' + id,
    method: 'get'
  })
}

// 新增出入库单据明细
export function addStockitem(data) {
  return request({
    url: '/goods/stockitem',
    method: 'post',
    data: data
  })
}

// 修改出入库单据明细
export function updateStockitem(data) {
  return request({
    url: '/goods/stockitem',
    method: 'put',
    data: data
  })
}

// 删除出入库单据明细
export function delStockitem(id) {
  return request({
    url: '/goods/stockitem/' + id,
    method: 'delete'
  })
}
