import request from '@/utils/request'

// 查询物品分类信息列表
export function listCategory(query) {
  return request({
    url: '/goods/category/list',
    method: 'get',
    params: query
  })
}

// 查询物品分类信息详细
export function getCategory(id) {
  return request({
    url: '/goods/category/' + id,
    method: 'get'
  })
}
// 查询物品分类信息详细
export function getCategoryChildren() {
  return request({
    url: '/goods/category' ,
    method: 'get'
  })
}
// 查询所有的上级分类
export function getParent() {
  return request({
    url: '/goods/category/parent' ,
    method: 'get'
  })
}

// 新增物品分类信息
export function addCategory(data) {
  return request({
    url: '/goods/category',
    method: 'post',
    data: data
  })
}

// 修改物品分类信息
export function updateCategory(data) {
  return request({
    url: '/goods/category',
    method: 'put',
    data: data
  })
}
// 迁移分类
export function removalCategory(data) {
  return request({
    url: '/goods/category/removal',
    method: 'post',
    data: data
  })
}

// 删除物品分类信息
export function delCategory(id) {
  return request({
    url: '/goods/category/' + id,
    method: 'delete'
  })
}
