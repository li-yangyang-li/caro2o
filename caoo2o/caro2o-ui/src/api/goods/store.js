import request from '@/utils/request'

// 查询仓库信息列表
export function listStore(query) {
  return request({
    url: '/goods/store/list',
    method: 'get',
    params: query
  })
}

// 查询仓库信息详细
export function getStore(id) {
  return request({
    url: '/goods/store/' + id,
    method: 'get'
  })
}

// 新增仓库信息
export function addStore(data) {
  return request({
    url: '/goods/store',
    method: 'post',
    data: data
  })
}

// 修改仓库信息
export function updateStore(data) {
  return request({
    url: '/goods/store',
    method: 'put',
    data: data
  })
}

// 删除仓库信息
export function delStore(id) {
  return request({
    url: '/goods/store/' + id,
    method: 'delete'
  })
}
