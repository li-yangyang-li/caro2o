import request from '@/utils/request'

// 查询物品信息列表
export function listRepertory(query) {
  return request({
    url: '/goods/repertory/list',
    method: 'get',
    params: query
  })
}

// 查询物品信息详细
export function getRepertory(id) {
  return request({
    url: '/goods/repertory/' + id,
    method: 'get'
  })
}

// 新增物品信息
export function addRepertory(data) {
  let formDate = new FormData()
  for (let dataKey in data) {
    formDate.append(dataKey,data[dataKey])
  }
  return request.post(
    '/goods/repertory' ,formDate,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
}

// 修改物品信息
export function updateRepertory(data) {
  let formDate = new FormData()
  for (let dataKey in data) {
    formDate.append(dataKey,data[dataKey])
  }
  return request.post(
    '/goods/repertory/update' ,formDate,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
}

// 删除物品信息
export function delRepertory(id) {
  return request({
    url: '/goods/repertory/' + id,
    method: 'delete'
  })
}
// 查询出入库明细
export function getOIstoreDetails(id) {
  return request({
    url: '/goods/repertory/details/' + id,
    method: 'get'
  })
}
