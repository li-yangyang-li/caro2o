import request from '@/utils/request'

// 查询套餐审核列表
export function listInfo(query) {
  return request({
    url: '/audit/info/list',
    method: 'get',
    params: query
  })
}
// 查询套餐审核列表
export function listTodoInfo() {
  return request({
    url: '/audit/info/todolist',
    method: 'get'
  })
}
//查询已办任务
export function listDoneInfo() {
  return request({
    url: '/audit/info/doneList',
    method: 'get'
  })
}

// 查询套餐审核详细
export function getInfo(id) {
  return request({
    url: '/audit/info/' + id,
    method: 'get'
  })
}
// 查询历史任务审核信息
export function getHistoryTask(id) {
  return request({
    url: '/audit/info/historyTask/' + id,
    method: 'get'
  })
}

// 新增套餐审核
export function addInfo(data) {
  return request({
    url: '/audit/info',
    method: 'post',
    data: data
  })
}
// 审批
export function audit(data) {
  return request({
    url: '/audit/info/audit',
    method: 'post',
    data: data
  })
}

// 修改套餐审核
export function updateInfo(data) {
  return request({
    url: '/audit/info',
    method: 'put',
    data: data
  })
}

// 删除套餐审核
export function delInfo(id) {
  return request({
    url: '/audit/info/' + id,
    method: 'delete'
  })
}
//查看审批流程图
export function getShowProgress(id) {
  return request({
    url: '/audit/info/showProgress/' + id,
    method: 'get'
  })

}

//保存审核信息
export function createProcessInstance(data) {
  return request({
    url: '/audit/info/createProcessInstance',
    method: 'post',
    data:data
  })
}
