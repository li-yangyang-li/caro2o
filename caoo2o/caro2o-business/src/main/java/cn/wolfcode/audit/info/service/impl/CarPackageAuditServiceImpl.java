package cn.wolfcode.audit.info.service.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import cn.wolfcode.audit.info.vo.AuditVO;
import cn.wolfcode.audit.info.vo.CarPackageAuditVO;
import cn.wolfcode.audit.info.vo.HistoryTask;
import cn.wolfcode.business.appointment.domain.ServiceItem;
import cn.wolfcode.business.appointment.service.IServiceItemService;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.PageUtils;
import cn.wolfcode.common.utils.SecurityUtils;
import cn.wolfcode.common.utils.bean.BeanUtils;
import cn.wolfcode.flow.bpmninfo.domain.BpmnInfo;
import cn.wolfcode.flow.bpmninfo.service.IBpmnInfoService;
import cn.wolfcode.flow.bpmninfo.service.IProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.audit.info.mapper.CarPackageAuditMapper;
import cn.wolfcode.audit.info.domain.CarPackageAudit;
import cn.wolfcode.audit.info.service.ICarPackageAuditService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import static cn.wolfcode.flow.bpmninfo.domain.BpmnInfo.BPMN_TYPE_AUDIT;

/**
 * 套餐审核Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-01-28
 */

@Service
public class CarPackageAuditServiceImpl implements ICarPackageAuditService 
{
    @Autowired
    private CarPackageAuditMapper carPackageAuditMapper;

    /**
     * 查询套餐审核
     * 
     * @param id 套餐审核主键
     * @return 套餐审核
     */
    @Override
    public CarPackageAudit selectCarPackageAuditById(Long id)
    {
        return carPackageAuditMapper.selectCarPackageAuditById(id);
    }

    /**
     * 查询套餐审核列表
     * 
     * @param carPackageAudit 套餐审核
     * @return 套餐审核
     */
    @Override
    public List<CarPackageAudit> selectCarPackageAuditList(CarPackageAudit carPackageAudit)
    {
        return carPackageAuditMapper.selectCarPackageAuditList(carPackageAudit);
    }

    @Autowired
    private IServiceItemService serviceItemService;


    @Autowired
    private IProcessService processService;

    @Autowired
    private IBpmnInfoService bpmnInfoService;
    /**
     * 新增套餐审核
     * 开启流程实例
     *
     * @param carPackageAuditvo 套餐审核
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertCarPackageAudit(CarPackageAuditVO carPackageAuditvo)
    {
        Assert.notNull(carPackageAuditvo,"参数错误");
        Assert.notNull(carPackageAuditvo.getServiceItemId(),"服务项Id不能为空");
        //通过服务项id查询服务项
        ServiceItem serviceItem = serviceItemService.selectServiceItemById(carPackageAuditvo.getServiceItemId().longValue());
        Assert.notNull(serviceItem,"服务项不存在");
        //查询服务项的价格
        BigDecimal discountPrice = serviceItem.getDiscountPrice();

        //获取店长id和财务id
        HashMap<String, Object> variables = new HashMap<>();
        variables.put("manageId",carPackageAuditvo.getShopOwnerId());
        variables.put("financeId",carPackageAuditvo.getFinancialId());
        variables.put("discountAmount",serviceItem.getDiscountPrice().doubleValue());
        //查询最新的bpmnInfo
        BpmnInfo bpmnInfo = bpmnInfoService.getLastBpmnInfo(BPMN_TYPE_AUDIT);

        CarPackageAudit carPackageAudit = getCarPackageAudit(carPackageAuditvo, serviceItem,discountPrice,serviceItem.getId());
        carPackageAuditMapper.insertCarPackageAudit(carPackageAudit);

        //开启流程实例
        //使用流程审核信息的id作为业务key
        String instanceId = processService.starttProcessInstance(bpmnInfo.getProcessDefinitionKey(),variables,carPackageAudit.getId());

        //将instanceid更新到审核信息表中
        CarPackageAudit carPackageAudit1 = new CarPackageAudit();
        carPackageAudit1.setId(carPackageAudit.getId());
        carPackageAudit1.setInstanceId(instanceId);

        carPackageAuditMapper.updateCarPackageAudit(carPackageAudit1);
        //修改当前服务项的审核状态
        int row = serviceItemService.updateStatus(serviceItem.getId(),ServiceItem.AUDIT_STATUS_AUDIT);
        Assert.state(row > 0,"服务项状态更新失败");
    }

    private static CarPackageAudit getCarPackageAudit(CarPackageAuditVO carPackageAuditvo, ServiceItem serviceItem, BigDecimal discountPrice,Long sid) {
        CarPackageAudit carPackageAudit = new CarPackageAudit();
        BeanUtils.copyBeanProp(carPackageAudit, carPackageAuditvo);
        carPackageAudit.setCreatorId(SecurityUtils.getUserId().toString());
        carPackageAudit.setServiceItemName(serviceItem.getName());
        carPackageAudit.setServiceItemInfo(serviceItem.getInfo());
        carPackageAudit.setCreateTime(DateUtils.getNowDate());
        carPackageAudit.setServiceItemPrice(discountPrice);
        carPackageAudit.setStatus(CarPackageAudit.AUDIT_STATUS_WAY);
        carPackageAudit.setServiceItemId(serviceItem.getId());
        return carPackageAudit;
    }

    /**
     * 修改套餐审核
     * 
     * @param carPackageAudit 套餐审核
     * @return 结果
     */
    @Override
    public int updateCarPackageAudit(CarPackageAudit carPackageAudit)
    {
        return carPackageAuditMapper.updateCarPackageAudit(carPackageAudit);
    }

    /**
     * 批量删除套餐审核
     * 
     * @param ids 需要删除的套餐审核主键
     * @return 结果
     */
    @Override
    public int deleteCarPackageAuditByIds(Long ids)
    {
        return carPackageAuditMapper.deleteCarPackageAuditByIds(ids);
    }

    /**
     * 删除套餐审核信息
     * 
     * @param id 套餐审核主键
     * @return 结果
     */
    @Override
    public int deleteCarPackageAuditById(Long id)
    {
        //状态判断
        Assert.notNull(id,"参数错误");
        CarPackageAudit carPackageAudit = carPackageAuditMapper.selectCarPackageAuditById(id);
        Assert.notNull(carPackageAudit,"审核信息不存在");
        Assert.state(carPackageAudit.getStatus() == CarPackageAudit.AUDIT_STATUS_WAY,"只有审核中才能撤销");
        //删除流程实例
        processService.removeInstance(carPackageAudit.getInstanceId(),SecurityUtils.getLoginUser().getUser().getNickName()+"撤销了申请");
        //更新审核状态,为以撤销
        carPackageAuditMapper.updateStatus(id,CarPackageAudit.AUDIT_STATUS_ANNUL);
        //更新服务项的状态,设置为初始化
        int row = serviceItemService.updateStatus(carPackageAudit.getServiceItemId(),ServiceItem.AUDIT_STATUS_INIT);
        Assert.state(row >0,"服务项状态更新失败");
        //删除流程定义的信息
        return carPackageAuditMapper.deleteCarPackageAuditById(id);
    }

    @Override
    public InputStream getShowProgress(String id) {
        Assert.notNull(id,"参数错误");
        return processService.getShowProgress(id);
    }

    @Override
    public List<CarPackageAudit> selectCarPackagetodolist() {

        BpmnInfo bpmnInfo = bpmnInfoService.getLastBpmnInfo(BPMN_TYPE_AUDIT);
        PageUtils.startPage();
        List<Long> CarPackageAuditIds = processService.selectTodoList(bpmnInfo.getProcessDefinitionKey());
        if (CarPackageAuditIds.size() ==0 ){
            return Collections.emptyList();
        }
        return carPackageAuditMapper.selectCarPackageAuditByIds(CarPackageAuditIds);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void audit(AuditVO auditvo) {
        Assert.notNull(auditvo.getId(),"参数错误");
        CarPackageAudit carPackageAudit = carPackageAuditMapper.selectCarPackageAuditById(auditvo.getId());
        Assert.notNull(carPackageAudit,"当前审核信息不存在");
        Assert.state(carPackageAudit.getStatus() == CarPackageAudit.AUDIT_STATUS_WAY,"当前状态不能审批");
        boolean complete = processService.complete(auditvo,carPackageAudit.getInstanceId());
        if (complete) {
            //更新审核信息的状态
            carPackageAuditMapper.updateStatus(auditvo.getId(),
                    auditvo.getPassed() ?CarPackageAudit.AUDIT_STATUS_PASSED:CarPackageAudit.AUDIT_STATUS_ANNUL);
            //更新服务项的状态
            //查询服务项
            serviceItemService.updateStatus(carPackageAudit.getServiceItemId(),
                    auditvo.getPassed() ?CarPackageAudit.AUDIT_STATUS_PASSED:CarPackageAudit.AUDIT_STATUS_ANNUL);
        }
    }

    @Override
    public List<HistoryTask> getHistoryTask(Long id) {
        Assert.notNull(id,"参数错误");
        //查询审核信息
        CarPackageAudit carPackageAudit = carPackageAuditMapper.selectCarPackageAuditById(id);
        Assert.notNull(carPackageAudit,"当前审核信息不存在");
        List<HistoryTask> tasks = processService.getHistoryTask(carPackageAudit.getInstanceId());
        return tasks;
    }

    /**
     * 查询已办任务
     *
     * @return
     */
    @Override
    public List<CarPackageAudit> selectCarPackageDonelist() {
        BpmnInfo bpmnInfo = bpmnInfoService.getLastBpmnInfo(BPMN_TYPE_AUDIT);
        List<Long> businessKey = processService.selectDoneList(bpmnInfo.getProcessDefinitionKey());
        if (businessKey.size() ==0 ){
            return Collections.emptyList();
        }
        //通过业务标识查询审核信息
        return carPackageAuditMapper.selectCarPackageAuditByIds(businessKey);

    }
}
