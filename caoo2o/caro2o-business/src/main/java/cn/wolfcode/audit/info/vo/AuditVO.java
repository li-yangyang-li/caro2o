package cn.wolfcode.audit.info.vo;

import cn.wolfcode.common.annotation.Excel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * 套餐审核对象 bus_car_package_audit
 * 
 * @author wolfcode
 * @date 2024-01-28
 */
@Getter
@Setter
@ToString
public class AuditVO
{

    private Long id;

    @NotNull(message = "审批意见不能为空")
    private Boolean passed;

    @NotNull(message = "批注不能为空")
    private String info;


}
