package cn.wolfcode.audit.info.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import cn.wolfcode.audit.info.vo.AuditVO;
import cn.wolfcode.audit.info.vo.CarPackageAuditVO;
import cn.wolfcode.audit.info.vo.HistoryTask;
import org.apache.commons.io.IOUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.audit.info.domain.CarPackageAudit;
import cn.wolfcode.audit.info.service.ICarPackageAuditService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 套餐审核Controller
 * 
 * @author wolfcode
 * @date 2024-01-28
 */
@RestController
@RequestMapping("/audit/info")
public class CarPackageAuditController extends BaseController
{
    @Autowired
    private ICarPackageAuditService carPackageAuditService;

    /**
     * 查询套餐审核列表
     */
    @PreAuthorize("@ss.hasPermi('audit:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(CarPackageAudit carPackageAudit)
    {
        startPage();
        List<CarPackageAudit> list = carPackageAuditService.selectCarPackageAuditList(carPackageAudit);
        return getDataTable(list);
    }
    /**
     * 查询套餐审核列表
     */
    @PreAuthorize("@ss.hasPermi('audit:info:list')")
    @GetMapping("/todolist")
    public TableDataInfo todolist()
    {

        List<CarPackageAudit> list = carPackageAuditService.selectCarPackagetodolist();
        return getDataTable(list);
    }
    /**
     * 查询已办任务
     */
    @PreAuthorize("@ss.hasPermi('audit:info:list')")
    @GetMapping("/doneList")
    public TableDataInfo doneList()
    {
        List<CarPackageAudit> list = carPackageAuditService.selectCarPackageDonelist();
        return getDataTable(list);
    }

    /**
     * 导出套餐审核列表
     */
    @PreAuthorize("@ss.hasPermi('audit:info:export')")
    @Log(title = "套餐审核", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarPackageAudit carPackageAudit)
    {
        List<CarPackageAudit> list = carPackageAuditService.selectCarPackageAuditList(carPackageAudit);
        ExcelUtil<CarPackageAudit> util = new ExcelUtil<CarPackageAudit>(CarPackageAudit.class);
        util.exportExcel(response, list, "套餐审核数据");
    }

    /**
     * 获取套餐审核详细信息
     */
    @PreAuthorize("@ss.hasPermi('audit:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(carPackageAuditService.selectCarPackageAuditById(id));
    }

    /**
     * 新增套餐审核
     */
    @PreAuthorize("@ss.hasPermi('audit:info:add')")
    @Log(title = "套餐审核", businessType = BusinessType.INSERT)
    @PostMapping("/createProcessInstance")
    public AjaxResult add(@RequestBody CarPackageAuditVO carPackageAudit)
    {
        carPackageAuditService.insertCarPackageAudit(carPackageAudit);
        return success();
    }

    /**
     * 套餐审批
     */
    @PreAuthorize("@ss.hasPermi('audit:info:edit')")
    @Log(title = "套餐审核", businessType = BusinessType.INSERT)
    @PostMapping("/audit")
    public AjaxResult audit(@RequestBody @Valid AuditVO auditvo)
    {
        carPackageAuditService.audit(auditvo);
        return success();
    }

    /**
     * 修改套餐审核
     */
    @PreAuthorize("@ss.hasPermi('audit:info:edit')")
    @Log(title = "套餐审核", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarPackageAudit carPackageAudit)
    {
        return toAjax(carPackageAuditService.updateCarPackageAudit(carPackageAudit));
    }

    /**
     * 删除套餐审核
     */
    @PreAuthorize("@ss.hasPermi('audit:info:remove')")
    @Log(title = "套餐审核", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(carPackageAuditService.deleteCarPackageAuditById(id));
    }

    /**
     * 获取流程进度
     */
    @PreAuthorize("@ss.hasPermi('audit:info:query')")
    @GetMapping("/showProgress/{id}")
    public void getShowProgress(@PathVariable("id") String id, HttpServletResponse response) throws IOException {
        InputStream inputStream = carPackageAuditService.getShowProgress(id);
        //将流文件使用HttpServletResponse响应给前端
        IOUtils.copy(inputStream,response.getOutputStream());
    }

    /**
     * 查询任务历史信息
     */
    @PreAuthorize("@ss.hasPermi('audit:info:query')")
    @GetMapping("/historyTask/{id}")
    public AjaxResult getHistoryTask(@PathVariable("id") Long id)  {
        List<HistoryTask> historyTask = carPackageAuditService.getHistoryTask(id);
        return success(historyTask);
    }
}
