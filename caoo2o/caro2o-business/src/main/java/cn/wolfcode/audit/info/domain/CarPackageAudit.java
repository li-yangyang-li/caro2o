package cn.wolfcode.audit.info.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 套餐审核对象 bus_car_package_audit
 * 
 * @author wolfcode
 * @date 2024-01-28
 */
public class CarPackageAudit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    //审核中
    public static final Integer AUDIT_STATUS_WAY = 0;
    public static final Integer AUDIT_STATUS_AUDITING = 1;
    public static final Integer AUDIT_STATUS_ANNUL = 3;
    public static final Integer AUDIT_STATUS_PASSED = 2;

    /** 主键 */
    private Long id;

    /** 服务项id */
    @Excel(name = "服务项id")
    private Long serviceItemId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String serviceItemName;

    /** 服务项备注 */
    @Excel(name = "服务项备注")
    private String serviceItemInfo;

    /** 服务项审核价格 */
    @Excel(name = "服务项审核价格")
    private BigDecimal serviceItemPrice;

    /** 流程实例id */
    @Excel(name = "流程实例id")
    private String instanceId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creatorId;

    /** 备注 */
    @Excel(name = "备注")
    private String info;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setServiceItemId(Long serviceItemId) 
    {
        this.serviceItemId = serviceItemId;
    }

    public Long getServiceItemId() 
    {
        return serviceItemId;
    }
    public void setServiceItemName(String serviceItemName) 
    {
        this.serviceItemName = serviceItemName;
    }

    public String getServiceItemName() 
    {
        return serviceItemName;
    }
    public void setServiceItemInfo(String serviceItemInfo) 
    {
        this.serviceItemInfo = serviceItemInfo;
    }

    public String getServiceItemInfo() 
    {
        return serviceItemInfo;
    }
    public void setServiceItemPrice(BigDecimal serviceItemPrice) 
    {
        this.serviceItemPrice = serviceItemPrice;
    }

    public BigDecimal getServiceItemPrice() 
    {
        return serviceItemPrice;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }
    public void setCreatorId(String creatorId) 
    {
        this.creatorId = creatorId;
    }

    public String getCreatorId() 
    {
        return creatorId;
    }
    public void setInfo(String info) 
    {
        this.info = info;
    }

    public String getInfo() 
    {
        return info;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("serviceItemId", getServiceItemId())
            .append("serviceItemName", getServiceItemName())
            .append("serviceItemInfo", getServiceItemInfo())
            .append("serviceItemPrice", getServiceItemPrice())
            .append("instanceId", getInstanceId())
            .append("creatorId", getCreatorId())
            .append("info", getInfo())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .toString();
    }
}
