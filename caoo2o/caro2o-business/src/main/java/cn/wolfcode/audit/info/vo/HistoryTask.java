package cn.wolfcode.audit.info.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 * @author zslstart
 * @Date 2024-01-31 15:12
 */
@Getter
@Setter
@ToString
public class HistoryTask {
    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    private String duration;

    private String comment;

}
