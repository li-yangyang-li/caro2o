package cn.wolfcode.audit.info.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 套餐审核对象 bus_car_package_audit
 * 
 * @author wolfcode
 * @date 2024-01-28
 */
@Getter
@Setter
@ToString
public class CarPackageAuditVO
{

    /** 服务项id */
    @Excel(name = "服务项id")
    private Integer serviceItemId;

    /** 备注 */
    @Excel(name = "备注")
    private String info;

    //店长id
    private Long shopOwnerId;
    //财务id
    private Long financialId;


}
