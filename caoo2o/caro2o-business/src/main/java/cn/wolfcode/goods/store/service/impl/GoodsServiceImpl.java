package cn.wolfcode.goods.store.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import cn.wolfcode.common.utils.bean.BeanUtils;
import cn.wolfcode.goods.store.vo.GoodsVO;
import cn.wolfcode.goods.store.vo.StockDetail;
import cn.wolfcode.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.goods.store.mapper.GoodsMapper;
import cn.wolfcode.goods.store.domain.Goods;
import cn.wolfcode.goods.store.service.IGoodsService;
import org.springframework.util.Assert;

/**
 * 物品信息Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-02-04
 */
@Service
public class GoodsServiceImpl implements IGoodsService 
{
    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 查询物品信息
     * 
     * @param id 物品信息主键
     * @return 物品信息
     */
    @Override
    public Goods selectGoodsById(Long id)
    {
        return goodsMapper.selectGoodsById(id);
    }

    /**
     * 查询物品信息列表
     * 
     * @param goods 物品信息
     * @return 物品信息
     */
    @Override
    public List<Goods> selectGoodsList(Goods goods)
    {
        return goodsMapper.selectGoodsList(goods);
    }


    @Autowired
    private ISysConfigService configService;
    /**
     * 新增物品信息
     * 
     * @param goodsvo 物品信息
     * @return 结果
     */
    @Override
    public int insertGoods(GoodsVO goodsvo)
    {
        //后续判断文件是否符合指定的文件类型
        //获取文件名
        String name = goodsvo.getName();
        Assert.notNull(name,"未上传图片");
        //使用UUID通用唯一识别码 + 后缀名的形式
        //设置唯一文件路径 防止文件名重复 出现覆盖的情况
        String fileName = UUID.randomUUID().toString() + name.substring(name.lastIndexOf("."));
        //打印查看
        System.out.println("唯一文件名：" + fileName);
        String uploadPath = configService.selectConfigByKey("img.upload.path");
        // 指定文件保存的路径
        String filePath = uploadPath + File.separator + fileName;
        //文件名保存到对应数据的头像图片字段
        Goods goods = new Goods();
        BeanUtils.copyBeanProp(goods,goodsvo);
        goods.setGoodsCover(fileName);
        int uploadPhoto = goodsMapper.insertGoods(goods);
        //将图片保存到本地
        //为图片生成名称
        //保存封面信息为图片的名称
        //图片路径保存到数据库表成功之后执行  将图片放入对应路径
        if (uploadPhoto > 0) {
            //根据上传路径创建文件夹File对象
            File saveAddress = new File(uploadPath);
            if (!saveAddress.exists()) {
                saveAddress.mkdirs();// 如果文件夹不存在 创建保存文件对应的文件夹
            }
            try {
                // 将上传的文件保存到指定路径
                goodsvo.getBpmnFile().transferTo(new File(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 1;
    }

    /**
     * 修改物品信息
     * 
     * @param goodsVO 物品信息
     * @return 结果
     */
    @Override
    public int updateGoods(GoodsVO goodsvo)
    {
        //后续判断文件是否符合指定的文件类型
        //获取文件名
        String name = goodsvo.getName();
        Assert.notNull(name,"未上传图片");
        //使用UUID通用唯一识别码 + 后缀名的形式
        //设置唯一文件路径 防止文件名重复 出现覆盖的情况
        String fileName = UUID.randomUUID().toString() + name.substring(name.lastIndexOf("."));
        //打印查看
        System.out.println("唯一文件名：" + fileName);
        String uploadPath = configService.selectConfigByKey("img.upload.path");
        // 指定文件保存的路径
        String filePath = uploadPath + File.separator + fileName;
        //文件名保存到对应数据的头像图片字段
        Goods goods = new Goods();
        BeanUtils.copyBeanProp(goods,goodsvo);
        goods.setGoodsCover(fileName);
        int uploadPhoto = goodsMapper.updateGoods(goods);
        //将图片保存到本地
        //为图片生成名称
        //保存封面信息为图片的名称
        //图片路径保存到数据库表成功之后执行  将图片放入对应路径
        if (uploadPhoto > 0) {
            //根据上传路径创建文件夹File对象
            File saveAddress = new File(uploadPath);
            if (!saveAddress.exists()) {
                saveAddress.mkdirs();// 如果文件夹不存在 创建保存文件对应的文件夹
            }
            try {
                // 将上传的文件保存到指定路径
                goodsvo.getBpmnFile().transferTo(new File(filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 1;
    }

    /**
     * 批量删除物品信息
     * 
     * @param ids 需要删除的物品信息主键
     * @return 结果
     */
    @Override
    public int deleteGoodsByIds(Long[] ids)
    {
        return goodsMapper.deleteGoodsByIds(ids);
    }

    /**
     * 删除物品信息信息
     * 
     * @param id 物品信息主键
     * @return 结果
     */
    @Override
    public int deleteGoodsById(Long id)
    {
        return goodsMapper.deleteGoodsById(id);
    }

    @Override
    public void updateGoodsStore(Long storeId, Long id, Long amounts) {
        goodsMapper.updateGoodsStore(storeId,id,amounts);
    }

    @Override
    public void updateGoodsStoreAdd(Long storeId, Long id, Long amounts) {
        goodsMapper.updateGoodsStoreAdd(storeId,id,amounts);
    }

    @Override
    public void insertGoodsStore(Long storeId, Long id, Long amounts) {
        //先查询数据库中是否有该条数据
        int row = goodsMapper.getGoodsStore(storeId,id);
        if (row>0){
            //有修改
            goodsMapper.updateGoodsStoreAdd(storeId,id,amounts);
        }else {
            //没有直接插入
            goodsMapper.insertGoodsStore(storeId,id,amounts);
        }

    }

    @Override
    public List<StockDetail> getOIstoreDetails(Long id) {
        List<StockDetail> oIstoreDetails = goodsMapper.getOIstoreDetails(id);
       int i = 1;
        for (StockDetail oIstoreDetail : oIstoreDetails) {
            oIstoreDetail.setSerialNumber(i++);
        }
        return oIstoreDetails;
    }
}
