package cn.wolfcode.goods.store.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.goods.store.vo.GoodsVO;
import cn.wolfcode.goods.store.vo.StockDetail;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.goods.store.domain.Goods;
import cn.wolfcode.goods.store.service.IGoodsService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 物品信息Controller
 * 
 * @author wolfcode
 * @date 2024-02-04
 */
@RestController
@RequestMapping("/goods/repertory")
public class GoodsController extends BaseController
{
    @Autowired
    private IGoodsService goodsService;

    /**
     * 查询物品信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:repertory:list')")
    @GetMapping("/list")
    public TableDataInfo list(Goods goods)
    {
        startPage();
        List<Goods> list = goodsService.selectGoodsList(goods);
        return getDataTable(list);
    }

    /**
     * 导出物品信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:repertory:export')")
    @Log(title = "物品信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Goods goods)
    {
        List<Goods> list = goodsService.selectGoodsList(goods);
        ExcelUtil<Goods> util = new ExcelUtil<Goods>(Goods.class);
        util.exportExcel(response, list, "物品信息数据");
    }

    /**
     * 获取物品信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:repertory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(goodsService.selectGoodsById(id));
    }
    /**
     * 获取物品出入库明细
     */
    @PreAuthorize("@ss.hasPermi('goods:repertory:query')")
    @GetMapping(value = "/details/{id}")
    public AjaxResult getOIstoreDetails(@PathVariable("id") Long id)
    {
        List<StockDetail> stockDetail = goodsService.getOIstoreDetails(id);
        return success(stockDetail);
    }

    /**
     * 新增物品信息
     */
    @PreAuthorize("@ss.hasPermi('goods:repertory:add')")
    @Log(title = "物品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(GoodsVO goodsvo)
    {
        return toAjax(goodsService.insertGoods(goodsvo));
    }

    /**
     * 修改物品信息
     */
    @PreAuthorize("@ss.hasPermi('goods:repertory:edit')")
    @Log(title = "物品信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    public AjaxResult edit(GoodsVO goodsVO)
    {
        return toAjax(goodsService.updateGoods(goodsVO));
    }

    /**
     * 删除物品信息
     */
    @PreAuthorize("@ss.hasPermi('goods:repertory:remove')")
    @Log(title = "物品信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(goodsService.deleteGoodsByIds(ids));
    }
}
