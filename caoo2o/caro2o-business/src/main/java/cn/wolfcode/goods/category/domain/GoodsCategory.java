package cn.wolfcode.goods.category.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * 物品分类信息对象 goods_category
 *
 * @author wolfcode
 * @date 2024-02-03
 */
@Getter
@Setter
@ToString
public class GoodsCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 分类名称 */
    @Excel(name = "分类名称")
    @NotEmpty(message = "分类名称不能为空")
    private String categoryName;

    /** 描述 */
    @Excel(name = "描述")
    private String categoryDesc;

    /** id层级结构 */
    private String busiPath;

    /** 上级分类id */
    @Excel(name = "上级分类id")
    private Long parentId;

    //保存子菜单分类
    private List<GoodsCategory> children = new ArrayList<>();


}
