package cn.wolfcode.goods.category.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cn.wolfcode.common.utils.StringUtils;
import cn.wolfcode.goods.category.vo.GoodsCategoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.goods.category.mapper.GoodsCategoryMapper;
import cn.wolfcode.goods.category.domain.GoodsCategory;
import cn.wolfcode.goods.category.service.IGoodsCategoryService;
import org.springframework.util.Assert;

/**
 * 物品分类信息Service业务层处理
 *
 * @author wolfcode
 * @date 2024-02-03
 */
@Service
public class GoodsCategoryServiceImpl implements IGoodsCategoryService
{
    @Autowired
    private GoodsCategoryMapper goodsCategoryMapper;

    /**
     * 查询物品分类信息
     *
     * @param id 物品分类信息主键
     * @return 物品分类信息
     */
    @Override
    public GoodsCategory selectGoodsCategoryById(Long id)
    {
        return goodsCategoryMapper.selectGoodsCategoryById(id);
    }

    /**
     * 查询物品分类信息列表
     *
     * @param goodsCategory 物品分类信息
     * @return 物品分类信息
     */
    @Override
    public List<GoodsCategory> selectGoodsCategoryList(GoodsCategory goodsCategory)
    {
        List<GoodsCategory> goodsCategories = goodsCategoryMapper.selectGoodsCategoryList(goodsCategory);
        return goodsCategories;

    }

    /**
     * 新增物品分类信息
     *
     * @param goodsCategory 物品分类信息
     * @return 结果
     */
    @Override
    public int insertGoodsCategory(GoodsCategory goodsCategory)
    {
        return goodsCategoryMapper.insertGoodsCategory(goodsCategory);
    }

    /**
     * 修改物品分类信息
     *
     * @param goodsCategory 物品分类信息
     * @return 结果
     */
    @Override
    public int updateGoodsCategory(GoodsCategory goodsCategory)
    {
        Assert.notNull(goodsCategory.getId(),"当前物品不存在");
        return goodsCategoryMapper.updateGoodsCategory(goodsCategory);
    }

    /**
     * 批量删除物品分类信息
     *
     * @param ids 需要删除的物品分类信息主键
     * @return 结果
     */
    @Override
    public int deleteGoodsCategoryByIds(Long[] ids)
    {
        return goodsCategoryMapper.deleteGoodsCategoryByIds(ids);
    }

    /**
     * 删除物品分类信息信息
     *
     * @param id 物品分类信息主键
     * @return 结果
     */
    @Override
    public int deleteGoodsCategoryById(Long id)
    {
        Assert.notNull(id,"参数错误");
        //删除时先判断是否有子级分类
        //如果有全部删除
        //查询所有的子分类
        List<GoodsCategory> list = goodsCategoryMapper.selectGoodsCategoryByParentId(id);
        if (list != null){
            for (GoodsCategory goodsCategory : list) {
                //通过递归调用删除子级菜单
                deleteGoodsCategoryById(goodsCategory.getId());
            }
        }
        //删除分类时如当前分类连同子分类下的商品还有库存则不可以删除
        return goodsCategoryMapper.deleteGoodsCategoryById(id);
    }

    @Override
    public List<GoodsCategory> selectGoodsCategoryChildrenList() {
        //将物品分类进行处理
        //判断当前分类是否有上级分类
        //有上级分类则将它的上级分类获取到,将当前分类放入上级分类的children里面
        //1.查询所有的分类
        List<GoodsCategory> goodsCategories = goodsCategoryMapper.selectGoodsCategoryChildrenList();
        //2.将分类放入map集合中,key为id,value为分类
        HashMap<Long, GoodsCategory> map = new HashMap<>();

        //3.遍历所有的分类,将分类放入map中
        for (GoodsCategory category : goodsCategories) {
            map.put(category.getId(),category);
        }
        //4.判断所有的分类是否有上级分类
        //使用迭代器遍历,需要使用迭代器删除遍历的元素,否则会发生,并发修改异常
        Iterator<GoodsCategory> iterator = goodsCategories.iterator();
        while (iterator.hasNext()) {
            GoodsCategory category = iterator.next();
            if (category.getParentId() != null) {
                Long id = map.get(category.getParentId()).getId();
                //如果当前分类有上级分类
                if (category.getParentId() == id) {
                    //将当前分类放入上级分类的children中
                    GoodsCategory categoryChildren = map.get(category.getParentId());
                    categoryChildren.getChildren().add(category);
                    //最后删除当前的分类
                    iterator.remove();
                }
            }
        }
        return goodsCategories;
    }

    /**
     * 对分类进行处理
     * @param goodsCategories
     */
    private static void extracted(List<GoodsCategory> goodsCategories) {
        //2.将分类放入map集合中,key为id,value为分类
        HashMap<Long, GoodsCategory> map = new HashMap<>();

        //3.遍历所有的分类,将分类放入map中
        for (GoodsCategory category : goodsCategories) {
            map.put(category.getId(),category);
        }
        //4.判断所有的分类是否有上级分类
        //使用迭代器遍历,需要使用迭代器删除遍历的元素,否则会发生,并发修改异常
        Iterator<GoodsCategory> iterator = goodsCategories.iterator();
        while (iterator.hasNext()){
            GoodsCategory category = iterator.next();
            if (category.getParentId() != null) {
                Long id = map.get(category.getParentId()).getId();
                //如果当前分类有上级分类
                if (category.getParentId() == id) {
                    //将当前分类放入上级分类的children中
                    GoodsCategory categoryChildren = map.get(category.getParentId());
                    categoryChildren.getChildren().add(category);
                    //最后删除当前的分类
                    iterator.remove();
                }
            }
        }
    }

    @Override
    public List<GoodsCategory> getParent() {
        return goodsCategoryMapper.getParent();
    }

    @Override
    public int removalCategory(GoodsCategoryVO goodsCategoryvo) {
        Assert.notNull(goodsCategoryvo,"参数错误");
        //判断是否迁移子分类
        if (goodsCategoryvo.getIsChildren()){
            //修改当前分类的父id为目标id
            goodsCategoryMapper.removalCategoryAndChildren(goodsCategoryvo.getParentIdRes(),goodsCategoryvo.getParentIdDesc());
        }else {
            //修改当前分类的父id为目标id
            goodsCategoryMapper.removalCategoryAndChildren(goodsCategoryvo.getParentIdRes(),goodsCategoryvo.getParentIdDesc());
            //修改当前分类的子分类的父id为null
            goodsCategoryMapper.removalCategory(goodsCategoryvo.getParentIdDesc());
        }
        return 1;
    }
}
