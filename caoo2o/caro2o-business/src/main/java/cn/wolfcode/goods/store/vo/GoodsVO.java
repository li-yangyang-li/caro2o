package cn.wolfcode.goods.store.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * 物品信息对象 goods
 * 
 * @author wolfcode
 * @date 2024-02-04
 */
@Getter
@Setter
@ToString
public class GoodsVO
{
    /** 物品id */
    private Long id;

    /** 物品名称 */
    @Excel(name = "物品名称")
    private String goodsName;

    /** 封面 */
    @Excel(name = "封面")
    private String goodsCover;

    /** 分类 */
    @Excel(name = "分类")
    private Long categoryId;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 规格 */
    @Excel(name = "规格")
    private String spec;

    /** 描述 */
    @Excel(name = "描述")
    private String goodsDesc;

    private MultipartFile bpmnFile;


    public String getName(){
        //获取文件名称
        return bpmnFile.getOriginalFilename();
    }

    public InputStream getInputStream() throws IOException {

        return bpmnFile.getInputStream();

    }
    //关闭流
    public void close(InputStream inputStream){
        //最后关闭流
        try {
            if (inputStream != null)
                inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
