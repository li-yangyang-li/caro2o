package cn.wolfcode.goods.oistore.domain;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 出入库单据明细对象 stock_bill_item
 * 
 * @author wolfcode
 * @date 2024-02-05
 */
@Getter
@Setter
@ToString
public class StockBillItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 物品id */
    @Excel(name = "物品id")
    private Long goodsId;

    /** 数量 */
    @Excel(name = "数量")
    private Long amounts;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 单据id */
    @Excel(name = "单据id")
    private Long billId;

    /** 状态 */
    @Excel(name = "状态")
    private Integer state;
    private String goodsName;



}
