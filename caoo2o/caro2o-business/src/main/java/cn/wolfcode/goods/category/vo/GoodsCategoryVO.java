package cn.wolfcode.goods.category.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * 物品分类信息对象 goods_category
 *
 * @author wolfcode
 * @date 2024-02-03
 */
@Getter
@Setter
@ToString
public class GoodsCategoryVO
{
    private static final long serialVersionUID = 1L;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String categoryName;

    /** 描述 */
    @Excel(name = "描述")
    private String categoryDesc;

    /** id层级结构 */
    private String busiPath;

    /** 上级分类id */
    @Excel(name = "上级分类id")
    private Long parentId;

    //保存子菜单分类
    private List<GoodsCategoryVO> children = new ArrayList<>();
    private Long parentIdRes;
    private Long parentIdDesc;
    private Boolean isChildren;


}
