package cn.wolfcode.goods.store.mapper;

import java.util.List;
import cn.wolfcode.goods.store.domain.Goods;
import cn.wolfcode.goods.store.vo.StockDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 物品信息Mapper接口
 * 
 * @author wolfcode
 * @date 2024-02-04
 */
public interface GoodsMapper 
{
    /**
     * 查询物品信息
     * 
     * @param id 物品信息主键
     * @return 物品信息
     */
    public Goods selectGoodsById(Long id);

    /**
     * 查询物品信息列表
     * 
     * @param goods 物品信息
     * @return 物品信息集合
     */
    public List<Goods> selectGoodsList(Goods goods);

    /**
     * 新增物品信息
     * 
     * @param goods 物品信息
     * @return 结果
     */
    public int insertGoods(Goods goods);

    /**
     * 修改物品信息
     * 
     * @param goods 物品信息
     * @return 结果
     */
    public int updateGoods(Goods goods);

    /**
     * 删除物品信息
     * 
     * @param id 物品信息主键
     * @return 结果
     */
    public int deleteGoodsById(Long id);

    /**
     * 批量删除物品信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGoodsByIds(Long[] ids);

    void updateGoodsStore(@Param("storeId") Long storeId, @Param("id") Long id, @Param("amounts") Long amounts);

    void updateGoodsStoreAdd(@Param("storeId") Long storeId, @Param("id") Long id, @Param("amounts") Long amounts);

    void insertGoodsStore(@Param("storeId") Long storeId, @Param("id") Long id, @Param("amounts") Long amounts);

    List<StockDetail> getOIstoreDetails(Long id);

    int getGoodsStore(@Param("storeId") Long storeId, @Param("id") Long id);
}
