package cn.wolfcode.goods.oistore.service.impl;

import java.math.BigDecimal;
import java.util.List;

import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.SecurityUtils;
import cn.wolfcode.common.utils.bean.BeanUtils;
import cn.wolfcode.goods.category.service.IGoodsCategoryService;
import cn.wolfcode.goods.oistore.domain.StockBillItem;
import cn.wolfcode.goods.oistore.service.IStockBillItemService;
import cn.wolfcode.goods.oistore.vo.CategoryBill;
import cn.wolfcode.goods.oistore.vo.StockBillVO;
import cn.wolfcode.goods.store.domain.Goods;
import cn.wolfcode.goods.store.service.IGoodsService;
import cn.wolfcode.system.domain.SysConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.goods.oistore.mapper.StockBillMapper;
import cn.wolfcode.goods.oistore.domain.StockBill;
import cn.wolfcode.goods.oistore.service.IStockBillService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * 出入库单据Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-02-05
 */
@Service
public class StockBillServiceImpl implements IStockBillService 
{
    @Autowired
    private StockBillMapper stockBillMapper;
    @Autowired
    private IStockBillItemService stockBillItemService;

    /**
     * 查询出入库单据
     * 
     * @param id 出入库单据主键
     * @return 出入库单据
     */
    @Override
    public StockBill selectStockBillById(Long id)
    {
        StockBill stockBill = stockBillMapper.selectStockBillById(id);
        //通过出入库id查询明细
        List<StockBillItem> stockBillItems = stockBillItemService.selectStockBillItemByBillId(stockBill.getId());
        for (StockBillItem stockBillItem : stockBillItems) {
            Goods goods = goodsService.selectGoodsById(stockBillItem.getGoodsId());
            stockBillItem.setGoodsName(goods.getGoodsName());
        }
        stockBill.setStockBillItems(stockBillItems);
        return stockBill;
    }

    /**
     * 查询出入库单据列表
     * 
     * @param stockBill 出入库单据
     * @return 出入库单据
     */
    @Override
    public List<StockBill> selectStockBillList(StockBill stockBill)
    {
        return stockBillMapper.selectStockBillList(stockBill);
    }
    @Autowired
    private IGoodsService goodsService;


    /**
     * 新增出入库单据
     * 
     * @param stockBillvo 出入库单据
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertStockBill(StockBillVO stockBillvo)
    {
        List<CategoryBill> categoryBills = stockBillvo.getCategoryBills();
        Assert.notNull(categoryBills,"商品不能为空");
        Long amounts = 0l;
        BigDecimal totalMoney = BigDecimal.ZERO;

        StockBill stockBill = new StockBill();
        BeanUtils.copyBeanProp(stockBill,stockBillvo);
        //判断当前出入库的商品数量是否合法
        if (stockBillvo.getIsOI()){//入库
            for (CategoryBill categoryBill : categoryBills) {
                Assert.state(categoryBill.getAmounts()>0 ,"入库数量不能小于0");
                Assert.state(categoryBill.getPrice().compareTo(new BigDecimal(0)) > 0 ,"入库价格不能小于0");
                amounts += (categoryBill.getAmounts());
                //计算总金额,总数量
                totalMoney = totalMoney.add(categoryBill.getPrice().multiply(new BigDecimal(categoryBill.getAmounts())));

                //保存商品到仓库
                goodsService.insertGoodsStore(stockBillvo.getStoreId(),categoryBill.getId(),amounts);
            }
            //设置操作人,操作时间,设置类型 0入库,1出库
            stockBill.setOperatorId(SecurityUtils.getUserId());
            stockBill.setOperateDate(DateUtils.getNowDate());
            stockBill.setType(StockBill.INPUT_STORE);
            stockBillMapper.insertStockBill(stockBill);
        }else {
            //2.出库时判断出库的数量是否大于库存数
            for (CategoryBill categoryBill : categoryBills) {
                Assert.state(categoryBill.getAmounts()>0 ,"出库数量不能小于0");
                Assert.state(categoryBill.getPrice().compareTo(new BigDecimal(0)) > 0 ,"出库价格不能小于0");
                //从数据库中查询当前商品的库存信息
                Goods goods = goodsService.selectGoodsById(categoryBill.getId());
                Assert.state(goods.getAmounts() >= categoryBill.getAmounts(),"当前商品数量不足,无法出库");
                if (goods.getAmounts() >= categoryBill.getAmounts()){
                    //说明出库的数量小于等于商品的库存数量可以出库
                    amounts += (categoryBill.getAmounts());
                    //计算总金额,总数量
                    totalMoney = totalMoney.add(categoryBill.getPrice().multiply(new BigDecimal(categoryBill.getAmounts())));

                    //通过仓库id和商品id更新每种商品的数量
                    goodsService.updateGoodsStore(stockBillvo.getStoreId(),categoryBill.getId(),amounts);
                }
            }
            //设置操作人,操作时间
            stockBill.setOperatorId(SecurityUtils.getUserId());
            stockBill.setOperateDate(DateUtils.getNowDate());
            stockBill.setType(StockBill.OUTPUT_STORE);
            stockBillMapper.insertStockBill(stockBill);
        }

        for (CategoryBill categoryBill : categoryBills) {
            //保存每种商品的入库明细
            StockBillItem stockBillItem = new StockBillItem();
            stockBillItem.setAmounts(amounts);
            stockBillItem.setGoodsId(categoryBill.getId());
            stockBillItem.setPrice(categoryBill.getPrice());
            //设置单据id
            stockBillItem.setBillId(stockBill.getId());
            stockBillItemService.insertStockBillItem(stockBillItem);
        }
        //保存总数量和总金额
        stockBill.setAmounts(Math.toIntExact(amounts));
        stockBill.setTotalMoney(totalMoney);
        stockBillMapper.updateStockBill(stockBill);


        return 1;

    }

    /**
     * 修改出入库单据
     * 
     * @param stockBill 出入库单据
     * @return 结果
     */
    @Override
    public int updateStockBill(StockBill stockBill)
    {
        return stockBillMapper.updateStockBill(stockBill);
    }

    /**
     * 批量删除出入库单据
     * 
     * @param ids 需要删除的出入库单据主键
     * @return 结果
     */
    @Override
    public int deleteStockBillByIds(Long[] ids)
    {
        return stockBillMapper.deleteStockBillByIds(ids);
    }

    /**
     * 删除出入库单据信息
     * 
     * @param id 出入库单据主键
     * @return 结果
     */
    @Override
    public int deleteStockBillById(Long id)
    {
        return stockBillMapper.deleteStockBillById(id);
    }
}
