package cn.wolfcode.goods.store.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 物品出入库明细实体类
 * @author zslstart
 * @Date 2024-02-07 13:13
 */
@Getter
@Setter
@ToString
public class StockDetail {
    private Integer serialNumber;
    private Integer type;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operateDate;
    private String goodsName;
    private String brand;
    private Long storeId;
    private Integer amounts;
    private BigDecimal price;
}
