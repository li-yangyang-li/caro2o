package cn.wolfcode.goods.oistore.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author zslstart
 * @Date 2024-02-06 17:05
 */
@Getter
@Setter
@ToString
public class CategoryBill {
    @NotNull(message = "明细不能为空")
    private Long id;
    @NotEmpty(message = "商品名不能为空")
    private String goodsName;
    @NotEmpty(message = "价格不能为空")
    private BigDecimal price;
    @NotNull(message = "数量不能为空")
    private Long amounts;
    private BigDecimal totalMoney;
}
