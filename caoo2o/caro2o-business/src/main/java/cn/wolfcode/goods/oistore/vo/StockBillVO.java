package cn.wolfcode.goods.oistore.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 出入库单据对象 stock_bill
 * 
 * @author wolfcode
 * @date 2024-02-05
 */
@Getter
@Setter
@ToString
public class StockBillVO
{
    private static final long serialVersionUID = 1L;

    /** 单据流水号 */
    private Long id;

    /** 类型 */
    @Excel(name = "类型")
    private Integer type;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long storeId;

    /** 出入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date busiDate;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date operateDate;

    /** 操作人 */
    @Excel(name = "操作人")
    private Long operatorId;
    private String remark;
    private Long amounts;
    private BigDecimal totalMoney;

    private List<CategoryBill>  categoryBills = new ArrayList<>();
    //出库还是入库 true 入库 false 出库
    private Boolean isOI;

}
