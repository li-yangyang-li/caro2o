package cn.wolfcode.goods.store.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;

/**
 * 物品信息对象 goods
 * 
 * @author wolfcode
 * @date 2024-02-04
 */
@Getter
@Setter
@ToString
public class Goods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 物品名称 */
    @Excel(name = "物品名称")
    private String goodsName;

    /** 封面 */
    @Excel(name = "封面")
    private String goodsCover;

    /** 分类 */
    @Excel(name = "分类")
    private Long categoryId;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 规格 */
    @Excel(name = "规格")
    private String spec;

    /** 描述 */
    @Excel(name = "描述")
    private String goodsDesc;
    /**
     * 仓库id
     */
    private Long storeId;
    /**
     * 仓库的数量
     */
    private Long amounts;

}
