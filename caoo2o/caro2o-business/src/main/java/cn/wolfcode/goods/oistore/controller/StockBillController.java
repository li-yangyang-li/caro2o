package cn.wolfcode.goods.oistore.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.wolfcode.goods.oistore.vo.StockBillVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.goods.oistore.domain.StockBill;
import cn.wolfcode.goods.oistore.service.IStockBillService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 出入库单据Controller
 * 
 * @author wolfcode
 * @date 2024-02-05
 */
@RestController
@RequestMapping("/goods/oistore")
public class StockBillController extends BaseController
{
    @Autowired
    private IStockBillService stockBillService;

    /**
     * 查询出入库单据列表
     */
    @PreAuthorize("@ss.hasPermi('goods:oistore:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockBill stockBill)
    {
        startPage();
        List<StockBill> list = stockBillService.selectStockBillList(stockBill);
        return getDataTable(list);
    }

    /**
     * 导出出入库单据列表
     */
    @PreAuthorize("@ss.hasPermi('goods:oistore:export')")
    @Log(title = "出入库单据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockBill stockBill)
    {
        List<StockBill> list = stockBillService.selectStockBillList(stockBill);
        ExcelUtil<StockBill> util = new ExcelUtil<StockBill>(StockBill.class);
        util.exportExcel(response, list, "出入库单据数据");
    }

    /**
     * 获取出入库单据详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:oistore:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(stockBillService.selectStockBillById(id));
    }
    /**
     * 获取出入库单据明细
     */
    @PreAuthorize("@ss.hasPermi('goods:oistore:query')")
    @GetMapping(value = "/getBillDetail/{id}")
    public AjaxResult getBillDetail(@PathVariable("id") Long id)
    {
        return success(stockBillService.selectStockBillById(id));
    }

    /**
     * 新增出入库单据
     */
    @PreAuthorize("@ss.hasPermi('goods:oistore:add')")
    @Log(title = "出入库单据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockBillVO stockBillvo)
    {
        return toAjax(stockBillService.insertStockBill(stockBillvo));
    }

    /**
     * 修改出入库单据
     */
    @PreAuthorize("@ss.hasPermi('goods:oistore:edit')")
    @Log(title = "出入库单据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockBill stockBill)
    {
        return toAjax(stockBillService.updateStockBill(stockBill));
    }

    /**
     * 删除出入库单据
     */
    @PreAuthorize("@ss.hasPermi('goods:oistore:remove')")
    @Log(title = "出入库单据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockBillService.deleteStockBillByIds(ids));
    }
}
