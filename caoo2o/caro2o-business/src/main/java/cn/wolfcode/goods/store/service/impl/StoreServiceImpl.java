package cn.wolfcode.goods.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.goods.store.mapper.StoreMapper;
import cn.wolfcode.goods.store.domain.Store;
import cn.wolfcode.goods.store.service.IStoreService;

/**
 * 仓库信息Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-02-04
 */
@Service
public class StoreServiceImpl implements IStoreService 
{
    @Autowired
    private StoreMapper storeMapper;

    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    @Override
    public Store selectStoreById(Long id)
    {
        return storeMapper.selectStoreById(id);
    }

    /**
     * 查询仓库信息列表
     * 
     * @param store 仓库信息
     * @return 仓库信息
     */
    @Override
    public List<Store> selectStoreList(Store store)
    {
        return storeMapper.selectStoreList(store);
    }

    /**
     * 新增仓库信息
     * 
     * @param store 仓库信息
     * @return 结果
     */
    @Override
    public int insertStore(Store store)
    {
        return storeMapper.insertStore(store);
    }

    /**
     * 修改仓库信息
     * 
     * @param store 仓库信息
     * @return 结果
     */
    @Override
    public int updateStore(Store store)
    {
        return storeMapper.updateStore(store);
    }

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的仓库信息主键
     * @return 结果
     */
    @Override
    public int deleteStoreByIds(Long[] ids)
    {
        return storeMapper.deleteStoreByIds(ids);
    }

    /**
     * 删除仓库信息信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    @Override
    public int deleteStoreById(Long id)
    {
        return storeMapper.deleteStoreById(id);
    }
}
