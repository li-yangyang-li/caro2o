package cn.wolfcode.goods.category.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import cn.wolfcode.goods.category.vo.GoodsCategoryVO;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.goods.category.domain.GoodsCategory;
import cn.wolfcode.goods.category.service.IGoodsCategoryService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 物品分类信息Controller
 *
 * @author wolfcode
 * @date 2024-02-03
 */
@RestController
@RequestMapping("/goods/category")
public class GoodsCategoryController extends BaseController
{
    @Autowired
    private IGoodsCategoryService goodsCategoryService;

    /**
     * 查询物品分类信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(GoodsCategory goodsCategory)
    {
        startPage();
        List<GoodsCategory> list = goodsCategoryService.selectGoodsCategoryList(goodsCategory);
        return getDataTable(list);
    }
    /**
     * 查询物品分类层级信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:list')")
    @GetMapping()
    public AjaxResult getChildren()
    {
        List<GoodsCategory> list = goodsCategoryService.selectGoodsCategoryChildrenList();
        return success(list);
    }
    /**
     * 查询所有的上级分类
     */
    @PreAuthorize("@ss.hasPermi('goods:category:list')")
    @GetMapping("/parent")
    public AjaxResult getParent()
    {
        List<GoodsCategory> list = goodsCategoryService.getParent();
        return success(list);
    }

    /**
     * 导出物品分类信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:category:export')")
    @Log(title = "物品分类信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoodsCategory goodsCategory)
    {
        List<GoodsCategory> list = goodsCategoryService.selectGoodsCategoryList(goodsCategory);
        ExcelUtil<GoodsCategory> util = new ExcelUtil<GoodsCategory>(GoodsCategory.class);
        util.exportExcel(response, list, "物品分类信息数据");
    }

    /**
     * 获取物品分类信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(goodsCategoryService.selectGoodsCategoryById(id));
    }

    /**
     * 新增物品分类信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:add')")
    @Log(title = "物品分类信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Valid GoodsCategory goodsCategory)
    {
        return toAjax(goodsCategoryService.insertGoodsCategory(goodsCategory));
    }
    /**
     * 迁移分类
     */
    @PreAuthorize("@ss.hasPermi('goods:category:edit')")
    @Log(title = "物品分类信息", businessType = BusinessType.INSERT)
    @PostMapping("/removal")
    public AjaxResult removalCategory(@RequestBody GoodsCategoryVO goodsCategoryvo)
    {
        return toAjax(goodsCategoryService.removalCategory(goodsCategoryvo));
    }

    /**
     * 修改物品分类信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:edit')")
    @Log(title = "物品分类信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @Valid GoodsCategory goodsCategory)
    {
        return toAjax(goodsCategoryService.updateGoodsCategory(goodsCategory));
    }

    /**
     * 删除物品分类信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:remove')")
    @Log(title = "物品分类信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(goodsCategoryService.deleteGoodsCategoryById(id));
    }
}
