package cn.wolfcode.flow.bpmninfo.controller;

import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.core.page.TableDataInfo;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.flow.bpmninfo.domain.BpmnInfo;
import cn.wolfcode.flow.bpmninfo.service.IBpmnInfoService;
import cn.wolfcode.flow.bpmninfo.vo.BpmnInfoVO;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 流程定义明细Controller
 *
 * @author wolfcode
 * @date 2024-01-25
 */
@RestController
@RequestMapping("/flow/bpmninfo")
public class BpmnInfoController extends BaseController {
    @Autowired
    private IBpmnInfoService bpmnInfoService;

    /**
     * 查询流程定义明细列表
     */
    @PreAuthorize("@ss.hasPermi('flow:bpmninfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BpmnInfo bpmnInfo) {
        startPage();
        List<BpmnInfo> list = bpmnInfoService.selectBpmnInfoList(bpmnInfo);
        return getDataTable(list);
    }

    /**
     * 导出流程定义明细列表
     */
    @PreAuthorize("@ss.hasPermi('flow:bpmninfo:export')")
    @Log(title = "流程定义明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BpmnInfo bpmnInfo) {
        List<BpmnInfo> list = bpmnInfoService.selectBpmnInfoList(bpmnInfo);
        ExcelUtil<BpmnInfo> util = new ExcelUtil<BpmnInfo>(BpmnInfo.class);
        util.exportExcel(response, list, "流程定义明细数据");
    }

    /**
     * 获取流程定义明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('flow:bpmninfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(bpmnInfoService.selectBpmnInfoById(id));
    }

    /**
     * 获取流程定义的文件
     */
    @PreAuthorize("@ss.hasPermi('flow:bpmninfo:query')")
    @GetMapping(value = "/resource/{id}/{type}")
    public void getResourceFile(@PathVariable("id") Long id,@PathVariable String type, HttpServletResponse response) throws IOException {
        InputStream inputStream = bpmnInfoService.getResourceFile(id,type);
        //将流文件使用HttpServletResponse响应给前端
        IOUtils.copy(inputStream,response.getOutputStream());
    }


    /**
     * 部署流程定义
     */
    @PreAuthorize("@ss.hasPermi('flow:bpmninfo:deploy')")
    @Log(title = "流程定义明细", businessType = BusinessType.INSERT)
    @PostMapping("/deploymentBpmn")
    public AjaxResult deploy(@Valid BpmnInfoVO bpmnInfo) {
        bpmnInfoService.insertBpmnInfo(bpmnInfo);
        return success();
    }

    /**
     * 修改流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('flow:bpmninfo:edit')")
    @Log(title = "流程定义明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BpmnInfo bpmnInfo) {
        return toAjax(bpmnInfoService.updateBpmnInfo(bpmnInfo));
    }

    /**
     * 删除流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('flow:bpmninfo:remove')")
    @Log(title = "流程定义明细", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult deleteDeployment(@PathVariable Long id) {
        bpmnInfoService.deleteBpmnInfoByIds(id);
        return success();
    }

}
