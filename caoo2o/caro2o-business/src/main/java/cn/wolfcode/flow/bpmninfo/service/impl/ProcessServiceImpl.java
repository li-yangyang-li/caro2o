package cn.wolfcode.flow.bpmninfo.service.impl;

import cn.wolfcode.audit.info.vo.AuditVO;
import cn.wolfcode.audit.info.vo.HistoryTask;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.SecurityUtils;
import cn.wolfcode.common.utils.StringUtils;
import cn.wolfcode.common.utils.file.FileUtils;
import cn.wolfcode.flow.bpmninfo.domain.BpmnInfo;
import cn.wolfcode.flow.bpmninfo.service.IProcessService;
import cn.wolfcode.flow.bpmninfo.vo.BpmnInfoVO;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipInputStream;

import static cn.wolfcode.common.constant.Constants.*;

/**
 * @author zslstart
 * @Date 2024-01-27 10:26
 */
@Service
public class ProcessServiceImpl implements IProcessService {
    //springboot将常用的Service管理起来
    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;
    /**
     * 部署流程定义
     * @param bpmnInfovo 流程定义文件信息
     * @return  流程定义信息
     */
    @Override
    public BpmnInfo deploy(BpmnInfoVO bpmnInfovo) {
        // 文件类型检查
        String filename = bpmnInfovo.getName();
        int index = filename.lastIndexOf(".");
        String ext = filename.substring(index + 1);
        List<String> listExt = Arrays.asList(ZIP,XML,BPMN);
        Assert.state(listExt.contains(ext),"文件格式不正确");

        //创建流程部署对象
        DeploymentBuilder deployment = repositoryService.createDeployment();
        deployment.disableSchemaValidation(); //关闭xml的校验
        InputStream inputStream = null;

        if (ZIP.equals(ext)){
            // 如果是zip格式使用addzip...方法
            try {
                inputStream = bpmnInfovo.getInputStream();
                deployment.addZipInputStream(new ZipInputStream(inputStream));
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                bpmnInfovo.close(inputStream);
            }
        }else {
            // 如果不是zip使用addInput...方法
            try {
                inputStream = bpmnInfovo.getInputStream();
                deployment.addInputStream(filename, inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                bpmnInfovo.close(inputStream);
            }
        }

        //设置部署名称
        Deployment deploy = deployment.name(FileUtils.getNameNotSuffix(filename)).deploy();
        System.out.println(deploy);
        BpmnInfo bpmnInfo = new BpmnInfo();
        //设置部署的时间
        bpmnInfo.setDeployTime(deploy.getDeploymentTime());
        //查询部署对象
        //流程定义的key和流程的版本
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .deploymentId(deploy.getId()).singleResult();

        bpmnInfo.setProcessDefinitionKey(processDefinition.getKey());
        bpmnInfo.setVersion(processDefinition.getVersion());
        bpmnInfo.setBpmnLabel(processDefinition.getName());

        return bpmnInfo;
    }

    @Override
    public void  deleteDeployment(String key,Integer version) {
        //根据bpmninfo拿到流程定义的key和版本号
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(key)
                .processDefinitionVersion(version)
                .singleResult();
        //删除流程定义
        //普通删除,不使用级联删除
        repositoryService.deleteDeployment(processDefinition.getDeploymentId());

    }

    @Autowired
    private DefaultProcessDiagramGenerator generator;
    @Override
    public InputStream getResourceFile(String processDefinitionKey, Integer version, String type) {
        //根据bpmninfo拿到流程定义的key和版本号
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(processDefinitionKey)
                .processDefinitionVersion(version)
                .singleResult();

        if ("file".equals(type)){
            //获取部署id和资源名称获取流文件
            return repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), processDefinition.getResourceName());
        }else if ("img".equals(type)){
            //获取部署id和资源名称获取流文件

            return repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), processDefinition.getDiagramResourceName());
        }else {
            BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinition.getId());
            return generator.generateDiagram(bpmnModel,"宋体","宋体","宋体");

        }

    }

    @Override
    public BigDecimal getlimitDiscountPriceByKey(String key, Integer version) {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(key)
                .processDefinitionVersion(version)
                .singleResult();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinition.getId());
        Process process = bpmnModel.getProcessById(processDefinition.getKey());
        Collection<FlowElement> flowElements = process.getFlowElements();
        String substring = "";
        for (FlowElement flowElement : flowElements) {
            if (flowElement instanceof SequenceFlow){
                //强制类型转换
                SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                String conditionExpression = sequenceFlow.getConditionExpression();
                if (StringUtils.isNotEmpty(conditionExpression) && conditionExpression.contains("discountAmount>")){
                    int i = conditionExpression.indexOf(">");
                    substring = conditionExpression.substring(i+1, conditionExpression.length() - 1).trim();
                }
            }
        }
        return new BigDecimal(substring);
    }

    @Override
    public String starttProcessInstance(String processDefinitionKey, HashMap<String, Object> variables, Long BusinessKey) {
        ProcessInstance instance = runtimeService.startProcessInstanceByKey(processDefinitionKey,BusinessKey.toString(),variables);
        return instance.getProcessInstanceId();
    }

    @Override
    public void removeInstance(String instanceId, String s) {
        runtimeService.deleteProcessInstance(instanceId,s);
    }

    /**
     * 获取实例的流程图
     * @param id 前端传入流程实例的id
     * @return
     */
    @Override
    public InputStream getShowProgress(String id) {
        //通过流程实例查询到这种执行的实例
        Execution execution = runtimeService.createExecutionQuery()
                .executionId(id)
                .singleResult();
        System.out.println(execution);
        //通过实例获取到执行的活动
        List<String> activeActivityIds = runtimeService.getActiveActivityIds(execution.getId());
        if (activeActivityIds == null){
            //创建以空集合
            activeActivityIds = Collections.EMPTY_LIST;
        }
        ProcessInstance instance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(id).singleResult();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(instance.getProcessDefinitionId());

        return generator.generateDiagram(bpmnModel,activeActivityIds,Collections.EMPTY_LIST,"宋体","宋体","宋体");

    }
    @Autowired
    private TaskService taskService;


    @Override
    public List<Long> selectTodoList(String processDefinitionKey) {
        //查询待办任务
        //通过流程定义的key和任务的负责人id获取到当前流程定义下的负责人的所有的任务
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey(processDefinitionKey)
                .taskAssignee(SecurityUtils.getUserId().toString())
                .list();

        Stream<String> stringStream = list.stream().map(task -> {
            //通过任务获取流程实例的id
            String processInstanceId = task.getProcessInstanceId();
            //通过流程实例的id获取到流程实例
            ProcessInstance instance = runtimeService.createProcessInstanceQuery()
                    .processInstanceId(processInstanceId).singleResult();
            //通过流程实例获取到业务key
            return instance.getBusinessKey();
        });
        ArrayList<Long> longs = new ArrayList<>();
        List<String> collect = stringStream.collect(Collectors.toList());
        for (String s : collect) {
            if (s !=null){
               longs.add(Long.parseLong(s)) ;
            }
        }
        return longs;
    }

    @Override
    public boolean complete(AuditVO auditvo, String instanceId) {
        //1. 根据负责人,流程定义的key和业务表示查询到任务
        //根据流程实例id查询流程实例
        ProcessInstance instance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(instanceId).singleResult();
        List<Task> list = taskService.createTaskQuery()
                .taskAssignee(SecurityUtils.getUserId().toString())
                .processDefinitionKey(instance.getProcessDefinitionKey())
                .list();
        for (Task task : list) {

            ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                    .processInstanceId(task.getProcessInstanceId()).singleResult();

            if (processInstance.getBusinessKey() != null && processInstance.getBusinessKey().equals(auditvo.getId().toString())){
                //2. 给当前任务添加批注
                HashMap<String, Object> map = new HashMap<>();
                map.put("passed",auditvo.getPassed());
                taskService.addComment(task.getId(),instanceId, auditvo.getInfo());
                //3. 处理任务,任务id,设置变量根据审批意见
                taskService.complete(task.getId(),map);
                break;
            }
        }
        //返回当前流程实例是否接收
        ProcessInstance instances = runtimeService.createProcessInstanceQuery()
                .processInstanceId(instanceId).singleResult();
        return instances == null;
    }

    @Autowired
    private HistoryService historyService;
    @Override
    public List<HistoryTask> getHistoryTask(String instanceId) {
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(instanceId)
                .finished()
                .list();

        List<HistoryTask> collect = list.stream().map(historicTask -> {
            HistoryTask task = new HistoryTask();
            task.setName(historicTask.getName());
            task.setStartTime(historicTask.getStartTime());
            task.setEndTime(historicTask.getEndTime());
            task.setDuration(DateUtils.timeDistanceByMS(historicTask.getDurationInMillis()));
            if (StringUtils.isNotEmpty(historicTask.getDeleteReason())) {
                //如果有删除的原因,则将其设置为任务的批注
                task.setComment(historicTask.getDeleteReason());
            } else {
                //没有撤销,查询任务的批注
                List<Comment> taskComments = taskService.getTaskComments(historicTask.getId());
                task.setComment(taskComments.get(0).getFullMessage());
            }
            return task;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public List<Long> selectDoneList(String processDefinitionKey) {
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .finished()
                .processDefinitionKey(processDefinitionKey)
                .taskAssignee(SecurityUtils.getUserId().toString())
                .list();
        List<String> collect = list.stream().map(task -> {
            String processInstanceId = task.getProcessInstanceId();
            //查询某个实例的历史
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                    .processInstanceId(processInstanceId)
                    .singleResult();
            //获取业务标识
            return historicProcessInstance.getBusinessKey();
        }).collect(Collectors.toList());
        ArrayList<Long> longs = new ArrayList<>();
        for (String s : collect) {
            if (s !=null){
                longs.add(Long.parseLong(s)) ;
            }
        }
        return longs;
    }
}
