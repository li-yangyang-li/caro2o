package cn.wolfcode.flow.bpmninfo.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * 流程定义明细对象 bus_bpmn_info
 * 
 * @author wolfcode
 * @date 2024-01-25
 */
@Getter
@Setter
@ToString
public class BpmnInfoVO
{

  @NotNull(message = "流程类型不能为空")
  private Integer bpmnType;
  @NotEmpty(message = "描述信息不能为空")
  private String info;
  private MultipartFile bpmnFile;


  public String getName(){
    //获取文件名称
    return bpmnFile.getOriginalFilename();
  }

  public InputStream getInputStream() throws IOException {

    return bpmnFile.getInputStream();

  }
  //关闭流
  public void close(InputStream inputStream){
    //最后关闭流
    try {
      if (inputStream != null)
        inputStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
