package cn.wolfcode.flow.bpmninfo.service.impl;

import java.io.InputStream;
import java.util.List;

import cn.wolfcode.flow.bpmninfo.service.IProcessService;
import cn.wolfcode.flow.bpmninfo.vo.BpmnInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.flow.bpmninfo.mapper.BpmnInfoMapper;
import cn.wolfcode.flow.bpmninfo.domain.BpmnInfo;
import cn.wolfcode.flow.bpmninfo.service.IBpmnInfoService;
import org.springframework.util.Assert;

/**
 * 流程定义明细Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-01-25
 */
@Service
public class BpmnInfoServiceImpl implements IBpmnInfoService 
{
    @Autowired
    private BpmnInfoMapper bpmnInfoMapper;
    @Autowired
    private IProcessService processService;

    /**
     * 查询流程定义明细
     * 
     * @param id 流程定义明细主键
     * @return 流程定义明细
     */
    @Override
    public BpmnInfo selectBpmnInfoById(Long id)
    {
        return bpmnInfoMapper.selectBpmnInfoById(id);
    }

    /**
     * 查询流程定义明细列表
     * 
     * @param bpmnInfo 流程定义明细
     * @return 流程定义明细
     */
    @Override
    public List<BpmnInfo> selectBpmnInfoList(BpmnInfo bpmnInfo)
    {
        return bpmnInfoMapper.selectBpmnInfoList(bpmnInfo);
    }

    /**
     * 新增流程定义明细
     *
     * @param bpmnInfovo 流程定义明细
     */
    @Override
    public void insertBpmnInfo(BpmnInfoVO bpmnInfovo)
    {
        //1. 调用Activiti的API部署流程定义
        //将Activiti的api相关的代码抽取到另一个service中(起到解耦的作用)
        BpmnInfo bpmnInfo = processService.deploy(bpmnInfovo);
        bpmnInfo.setInfo(bpmnInfovo.getInfo());
        bpmnInfo.setBpmnType(bpmnInfovo.getBpmnType());
        //2. 将流程定义相关的数据封装到BpmnInfo对象中,保存到数据库中
        int i = bpmnInfoMapper.insertBpmnInfo(bpmnInfo);

        Assert.state(i>0,"保存流程定义信息失败");
    }

    /**
     * 修改流程定义明细
     * 
     * @param bpmnInfo 流程定义明细
     * @return 结果
     */
    @Override
    public int updateBpmnInfo(BpmnInfo bpmnInfo)
    {
        return bpmnInfoMapper.updateBpmnInfo(bpmnInfo);
    }

    /**
     * 批量删除流程定义明细
     *
     * @param id 需要删除的流程定义明细主键
     */
    @Override
    public void deleteBpmnInfoByIds(Long id)
    {
        Assert.notNull(id,"参数错误");
        //删除流程定义
        //通过id查询查询bpmn_info
        BpmnInfo bpmnInfo = bpmnInfoMapper.selectBpmnInfoById(id);
        Assert.notNull(bpmnInfo,"当前流程定义不存在");
        processService.deleteDeployment(bpmnInfo.getProcessDefinitionKey(),bpmnInfo.getVersion());
        bpmnInfoMapper.deleteBpmnInfoById(id);
    }

    /**
     * 删除流程定义明细信息
     * 
     * @param id 流程定义明细主键
     * @return 结果
     */
    @Override
    public int deleteBpmnInfoById(Long id)
    {
        return bpmnInfoMapper.deleteBpmnInfoById(id);
    }

    @Override
    public InputStream getResourceFile(Long id, String type) {
        Assert.notNull(id,"参数错误");
        //查询流程定义
        //通过id查询查询bpmn_info
        BpmnInfo bpmnInfo = bpmnInfoMapper.selectBpmnInfoById(id);
        Assert.notNull(bpmnInfo,"当前流程定义不存在");
        return processService.getResourceFile(bpmnInfo.getProcessDefinitionKey(),bpmnInfo.getVersion(),type);

    }

    @Override
    public BpmnInfo getLastBpmnInfo(Integer type) {
        return bpmnInfoMapper.getLastBpmnInfo(type);
    }


}
