package cn.wolfcode.flow.bpmninfo.service;

import cn.wolfcode.audit.info.vo.AuditVO;
import cn.wolfcode.audit.info.vo.HistoryTask;
import cn.wolfcode.flow.bpmninfo.domain.BpmnInfo;
import cn.wolfcode.flow.bpmninfo.vo.BpmnInfoVO;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * @author zslstart
 * @Date 2024-01-27 10:17
 */
public interface IProcessService {

    BpmnInfo deploy(BpmnInfoVO bpmnInfovo);

    void deleteDeployment(String key,Integer version);

    InputStream getResourceFile(String processDefinitionKey, Integer version, String type);

    BigDecimal getlimitDiscountPriceByKey(String key, Integer version);

    String starttProcessInstance(String processDefinitionKey, HashMap<String, Object> variables, Long id);

    void removeInstance(String instanceId, String s);

    InputStream getShowProgress(String id);

    List<Long> selectTodoList(String processDefinitionKey);

    boolean complete(AuditVO auditvo, String instanceId);

    List<HistoryTask> getHistoryTask(String instanceId);

    List<Long> selectDoneList(String processDefinitionKey);

}
