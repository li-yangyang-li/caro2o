package cn.wolfcode.flow.bpmninfo.service;

import java.io.InputStream;
import java.util.List;
import cn.wolfcode.flow.bpmninfo.domain.BpmnInfo;
import cn.wolfcode.flow.bpmninfo.vo.BpmnInfoVO;

/**
 * 流程定义明细Service接口
 * 
 * @author wolfcode
 * @date 2024-01-25
 */
public interface IBpmnInfoService 
{
    /**
     * 查询流程定义明细
     * 
     * @param id 流程定义明细主键
     * @return 流程定义明细
     */
    public BpmnInfo selectBpmnInfoById(Long id);

    /**
     * 查询流程定义明细列表
     * 
     * @param bpmnInfo 流程定义明细
     * @return 流程定义明细集合
     */
    public List<BpmnInfo> selectBpmnInfoList(BpmnInfo bpmnInfo);

    /**
     * 新增流程定义明细
     *
     * @param bpmnInfo 流程定义明细
     */
    public void insertBpmnInfo(BpmnInfoVO bpmnInfo);

    /**
     * 修改流程定义明细
     * 
     * @param bpmnInfo 流程定义明细
     * @return 结果
     */
    public int updateBpmnInfo(BpmnInfo bpmnInfo);

    /**
     * 批量删除流程定义明细
     *
     * @param id 需要删除的流程定义明细主键集合
     */
    public void deleteBpmnInfoByIds(Long id);

    /**
     * 删除流程定义明细信息
     * 
     * @param id 流程定义明细主键
     * @return 结果
     */
    public int deleteBpmnInfoById(Long id);

    InputStream getResourceFile(Long id, String type);

    BpmnInfo getLastBpmnInfo(Integer type);

}
