package cn.wolfcode.business.appointment.service;

import java.util.List;
import cn.wolfcode.business.appointment.domain.Statement;
import cn.wolfcode.business.appointment.domain.StatementItem;
import cn.wolfcode.business.appointment.vo.StatementVO;

/**
 * 结算单Service接口
 * 
 * @author wolfcode
 * @date 2024-01-22
 */
public interface IStatementService 
{
    /**
     * 查询结算单
     * 
     * @param id 结算单主键
     * @return 结算单
     */
    public Statement selectStatementById(Long id);

    /**
     * 查询结算单列表
     * 
     * @param statement 结算单
     * @return 结算单集合
     */
    public List<Statement> selectStatementList(Statement statement);

    /**
     * 新增结算单
     * 
     * @param statement 结算单
     * @return 结果
     */
    public int insertStatement(StatementVO statement);

    /**
     * 修改结算单
     * 
     * @param statement 结算单
     * @return 结果
     */
    public int updateStatement(StatementVO statement);

    /**
     * 批量删除结算单
     * 
     * @param ids 需要删除的结算单主键集合
     * @return 结果
     */
    public int deleteStatementByIds(Long[] ids);

    /**
     * 删除结算单信息
     * 
     * @param id 结算单主键
     * @return 结果
     */
    public int deleteStatementById(Long id);

    void saveItems(Statement statement);

    List<StatementItem> selectStatementItemList(Long id);

    void updateStatementPay(Long id);

    Statement createStatement(Long id);

}
