package cn.wolfcode.business.appointment.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.constant.Constants;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * 为了避免前端传入其他的字段(实体类中有所有的字段),并不是我们添加时需要的字段,所有需要创建VO对象,来接收我们需要的字段.
 * 
 * @author wolfcode
 * @date 2024-01-18
 */
public class AppointmentVO
{
    private static final long serialVersionUID = 1L;

    //预约中
    public static final int APPOINTING = 0;
    public static final int ARRIVED = 1;
    public static final int USER_CANCEL = 2;
    public static final int TIMEOUT_CANCEL = 3;
    public static final int SETTLED = 4;
    public static final int PAID = 5;


    /** $column.columnComment */
    private Long id;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    @NotEmpty(message = "客户姓名不能为空")
    private String customerName;

    /** 客户联系方式 */
    @Excel(name = "客户联系方式")
    @NotEmpty(message = "客户联系方式不能为空")
    @Pattern(regexp = Constants.PHONE_REGEX)
    private String customerPhone;

    /** 预约时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "预约时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "预约时间不能为空")
    private Date appointmentTime;


    /** 车牌号码 */
    @Excel(name = "车牌号码")
    @NotEmpty(message = "车牌号码不能为空")
    @Pattern(regexp = Constants.LICENSE_REGEX)
    private String licensePlate;

    /** 汽车类型 */
    @Excel(name = "汽车类型")
    @NotEmpty(message = "汽车类型不能为空")
    private String carSeries;

    /** 服务类型 */
    @Excel(name = "服务类型")
    @NotNull(message = "服务类型不能为空")
    private Integer serviceType;

    /** 备注信息 */
    @Excel(name = "备注信息")
    private String info;



    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerName()
    {
        return customerName;
    }
    public void setCustomerPhone(String customerPhone)
    {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhone()
    {
        return customerPhone;
    }
    public void setAppointmentTime(Date appointmentTime)
    {
        this.appointmentTime = appointmentTime;
    }

    public Date getAppointmentTime()
    {
        return appointmentTime;
    }



    public void setLicensePlate(String licensePlate)
    {
        this.licensePlate = licensePlate;
    }

    public String getLicensePlate()
    {
        return licensePlate;
    }
    public void setCarSeries(String carSeries)
    {
        this.carSeries = carSeries;
    }

    public String getCarSeries()
    {
        return carSeries;
    }
    public void setServiceType(Integer serviceType)
    {
        this.serviceType = serviceType;
    }

    public Integer getServiceType()
    {
        return serviceType;
    }
    public void setInfo(String info)
    {
        this.info = info;
    }

    public String getInfo()
    {
        return info;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerName", getCustomerName())
            .append("customerPhone", getCustomerPhone())
            .append("appointmentTime", getAppointmentTime())
            .append("licensePlate", getLicensePlate())
            .append("carSeries", getCarSeries())
            .append("serviceType", getServiceType())
            .append("info", getInfo())
            .toString();
    }
}
