package cn.wolfcode.business.appointment.domain;

import cn.wolfcode.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author zslstart
 * @Date 2024-01-24 11:52
 */
public class StatementItem {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 结算单ID */
    @Excel(name = "结算单ID")
    @NotNull(message = "结算单id不能为空")
    private Long statementId;

    /** 服务项明细ID */
    @Excel(name = "服务项明细ID")
    @NotNull(message = "服务项明细ID不能为空")
    private Long itemId;

    /** 服务项明细名称 */
    @Excel(name = "服务项明细名称")
    @NotEmpty(message = "服务项明细名称不能为空")
    private String itemName;

    /** 服务项价格 */
    @Excel(name = "服务项价格")
    @NotNull(message = "服务项价格不能为空")
    private BigDecimal itemPrice;

    /** 购买数量 */
    @Excel(name = "购买数量")
    @NotNull(message = "购买数量不能为空")
    private BigDecimal itemQuantity;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setStatementId(Long statementId)
    {
        this.statementId = statementId;
    }

    public Long getStatementId()
    {
        return statementId;
    }
    public void setItemId(Long itemId)
    {
        this.itemId = itemId;
    }

    public Long getItemId()
    {
        return itemId;
    }
    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getItemName()
    {
        return itemName;
    }
    public void setItemPrice(BigDecimal itemPrice)
    {
        this.itemPrice = itemPrice;
    }

    public BigDecimal getItemPrice()
    {
        return itemPrice;
    }

    public BigDecimal getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(BigDecimal itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("statementId", getStatementId())
                .append("itemId", getItemId())
                .append("itemName", getItemName())
                .append("itemPrice", getItemPrice())
                .append("itemQuantity", getItemQuantity())
                .toString();
    }
}
