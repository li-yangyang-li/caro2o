package cn.wolfcode.business.appointment.service;

import java.util.List;
import java.util.Map;

import cn.wolfcode.business.appointment.domain.ServiceItem;
import cn.wolfcode.business.appointment.vo.ServiceItemVO;

/**
 * 服务项Service接口
 * 
 * @author wolfcode
 * @date 2024-01-21
 */
public interface IServiceItemService 
{
    /**
     * 查询服务项
     * 
     * @param id 服务项主键
     * @return 服务项
     */
    public ServiceItem selectServiceItemById(Long id);

    /**
     * 查询服务项列表
     * 
     * @param serviceItem 服务项
     * @return 服务项集合
     */
    public List<ServiceItem> selectServiceItemList(ServiceItem serviceItem);

    /**
     * 新增服务项
     * 
     * @param serviceItem 服务项
     * @return 结果
     */
    public int insertServiceItem(ServiceItemVO serviceItem);

    /**
     * 修改服务项
     * 
     * @param serviceItem 服务项
     * @return 结果
     */
    public int updateServiceItem(ServiceItemVO serviceItem);

    /**
     * 批量删除服务项
     * 
     * @param ids 需要删除的服务项主键集合
     * @return 结果
     */
    public int deleteServiceItemByIds(Long[] ids);

    /**
     * 删除服务项信息
     * 
     * @param id 服务项主键
     * @return 结果
     */
    public int deleteServiceItemById(Long id);

    int updateSaleStatus(Long id);

    /**
     * 查询审核信息
     * @return 店长,财务
     */
    Map<String,Object> getAuditInfo(Long id);

    int updateStatus(Long id, Integer auditStatusAudit);

}
