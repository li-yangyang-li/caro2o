package cn.wolfcode.business.appointment.service;

import java.util.List;
import cn.wolfcode.business.appointment.domain.Appointment;
import cn.wolfcode.business.appointment.domain.Statement;
import cn.wolfcode.business.appointment.vo.AppointmentVO;

/**
 * 养修信息预约Service接口
 * 
 * @author wolfcode
 * @date 2024-01-18
 */
public interface IAppointmentService 
{
    /**
     * 查询养修信息预约
     * 
     * @param id 养修信息预约主键
     * @return 养修信息预约
     */
    public Appointment selectAppointmentById(Long id);

    /**
     * 查询养修信息预约列表
     * 
     * @param appointment 养修信息预约
     * @return 养修信息预约集合
     */
    public List<Appointment> selectAppointmentList(Appointment appointment);

    /**
     * 新增养修信息预约
     * 
     * @param appointment 养修信息预约
     * @return 结果
     */
    public int insertAppointment(AppointmentVO appointment);

    /**
     * 修改养修信息预约
     * 
     * @param appointment 养修信息预约
     * @return 结果
     */
    public int updateAppointment(AppointmentVO appointment);

    /**
     * 批量删除养修信息预约
     * 
     * @param ids 需要删除的养修信息预约主键集合
     * @return 结果
     */
    public int deleteAppointmentByIds(Long[] ids);

    /**
     * 删除养修信息预约信息
     * 
     * @param id 养修信息预约主键
     * @return 结果
     */
    public int deleteAppointmentById(Long id);

    public int patchAppointmentById(Long id);

    public int cancelAppointmentById(Long id);

    void updateAppointmentByStatus(Long appointmentId);



}
