package cn.wolfcode.business.appointment.service.impl;

import java.util.Date;
import java.util.List;

import cn.wolfcode.business.appointment.domain.Statement;
import cn.wolfcode.business.appointment.vo.AppointmentVO;
import cn.wolfcode.common.core.redis.RedisCache;
import cn.wolfcode.common.enums.AppointmentStatusEnum;
import cn.wolfcode.common.enums.AppointmentStatusEnum.*;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.bean.BeanUtils;
import cn.wolfcode.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.appointment.mapper.AppointmentMapper;
import cn.wolfcode.business.appointment.domain.Appointment;
import cn.wolfcode.business.appointment.service.IAppointmentService;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;


//导入类中的静态成员
import static cn.wolfcode.common.enums.AppointmentStatusEnum.*;


/**
 * 养修信息预约Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-01-18
 */
@Service
public class AppointmentServiceImpl implements IAppointmentService 
{
    @Autowired
    private AppointmentMapper appointmentMapper;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private ISysConfigService configService;

    /**
     * 查询养修信息预约
     * 
     * @param id 养修信息预约主键
     * @return 养修信息预约
     */
    @Override
    public Appointment selectAppointmentById(Long id)
    {
        return appointmentMapper.selectAppointmentById(id);
    }

    /**
     * 查询养修信息预约列表
     * 
     * @param appointment 养修信息预约
     * @return 养修信息预约
     */
    @Override
    public List<Appointment> selectAppointmentList(Appointment appointment)
    {
        return appointmentMapper.selectAppointmentList(appointment);
    }

    /**
     * 新增养修信息预约
     * 
     * @param appointment 养修信息预约
     * @return 结果
     */
    @Override
    public int insertAppointment(AppointmentVO appointment)
    {
        //预约时间限定
        Assert.state(DateUtils.differentDaysByMillisecondNew(new Date(),appointment.getAppointmentTime()) >= 1,"只能预约当天之后的时间");
        //预约次数限定
        //操作redis的预约次数
        //获取当天剩余的毫米数
        long timeout = DateUtils.getRestSecondsOfDay();
        int times = redisCache.increment("appointment_times:"+appointment.getCustomerPhone(),timeout);
        System.out.println("以预约:"+times);
        //查询预约次数的上限
        int maxTimes= 5;
        String maxTimesStr = configService.selectConfigByKey("user.app.max");
        System.out.println(maxTimesStr);
        if (StringUtils.hasLength(maxTimesStr)){
            maxTimes = Integer.parseInt(maxTimesStr);
        }
        System.out.println(maxTimes);
        Assert.state(times <= maxTimes,"当天最多预约"+maxTimes+"次");
        //将VO转换为domain实体类
        Appointment appointmentDest = new Appointment();
        BeanUtils.copyBeanProp(appointmentDest,appointment);
        //因为VO中没有CreateTime的字段,但是这里需要使用到这个字段,所有需要将VO转换为Appointment对象
        appointmentDest.setCreateTime(DateUtils.getNowDate());
        return appointmentMapper.insertAppointment(appointmentDest);
    }

    /**
     * 修改养修信息预约
     * 
     * @param appointmentvo 养修信息预约
     * @return 结果
     */
    @Override
    public int updateAppointment(AppointmentVO appointmentvo)
    {
        //如果当前预约单的状态为预约中才能修改
        //从数据库中通过id查询当前的预约单的状态
        Appointment app = appointmentMapper.selectAppointmentById(appointmentvo.getId());
        Assert.notNull(app,"当前预约单不存在");
        //0是魔法数字,可读性差; 解决方式:枚举类或常量
        //使用枚举类,AppointmentStatusEnum.APPOINTING.ordinal(),ordinal()方法获取到索引
        Assert.state(app.getStatus() == APPOINTING.ordinal(),"当前状态不允许修改");
        Appointment appointment = new Appointment();
        BeanUtils.copyBeanProp(appointment,appointmentvo);
        return appointmentMapper.updateAppointment(appointment);
    }

    /**
     * 批量删除养修信息预约
     * 
     * @param ids 需要删除的养修信息预约主键
     * @return 结果
     */
    @Override
    public int deleteAppointmentByIds(Long[] ids)
    {
        return appointmentMapper.deleteAppointmentByIds(ids);
    }

    /**
     * 删除养修信息预约信息
     * 
     * @param id 养修信息预约主键
     * @return 结果
     */
    @Override
    public int deleteAppointmentById(Long id)
    {
        return appointmentMapper.deleteAppointmentById(id);
    }

    /**
     * 修改预约单到店状态
     * @param id
     * @return
     */
    @Override
    public int patchAppointmentById(Long id) {
        Assert.notNull(id,"参数错误");
        //先通过id查询是否有订单
        Appointment app = appointmentMapper.selectAppointmentById(id);
        Assert.notNull(app,"当前预约单不存在");
        //再判断该订单状态是否为0
        Assert.state(app.getStatus() == APPOINTING.ordinal(),"当前状态不允许修改");
        //有订单,状态为0才能修改
        return appointmentMapper.patchAppointmentById(id);
    }

    /**
     * 取消预约单
     * @param id
     * @return
     */
    @Override
    public int cancelAppointmentById(Long id) {
        //先通过id查询是否有订单
        Appointment app = appointmentMapper.selectAppointmentById(id);
        Assert.notNull(app,"当前预约单不存在");
        //再判断该订单状态是否为0
        Assert.state(app.getStatus() == APPOINTING.ordinal(),"当前状态不允许修改");
        //有订单,状态为0才能修改
        return appointmentMapper.cancelAppointmentById(id);
    }

    @Override
    public void updateAppointmentByStatus(Long appointmentId) {
        appointmentMapper.updateAppointmentByStatus(appointmentId,AppointmentVO.PAID);
    }


}

