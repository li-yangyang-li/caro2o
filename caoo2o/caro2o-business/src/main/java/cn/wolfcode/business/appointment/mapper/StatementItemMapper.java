package cn.wolfcode.business.appointment.mapper;

import cn.wolfcode.business.appointment.domain.StatementItem;

import java.util.List;

/**
 * 结算单明细Mapper接口
 * 
 * @author wolfcode
 * @date 2024-01-22
 */
public interface StatementItemMapper 
{

    /**
     * 新增结算单明细
     * 
     * @param statementItem 结算单明细
     * @return 结果
     */
    public int insertStatementItem(List<StatementItem> statementItem);

    public int deleteByStatementId(Long statementId);

    public List<StatementItem> selectByStatementId(Long statementId);
}
