package cn.wolfcode.business.appointment.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author zslstart
 * @Date 2024-01-28 19:12
 */
@Getter
@Setter
@ToString
public class ShopOwner {
    private Integer userId;
    private String nickName;
}
