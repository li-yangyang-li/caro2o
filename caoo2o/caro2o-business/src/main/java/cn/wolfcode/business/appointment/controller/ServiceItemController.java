package cn.wolfcode.business.appointment.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import cn.wolfcode.business.appointment.vo.ServiceItemVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.appointment.domain.ServiceItem;
import cn.wolfcode.business.appointment.service.IServiceItemService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 服务项Controller
 *
 * @author wolfcode
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/appointment/serviceitem")
public class ServiceItemController extends BaseController
{
    @Autowired
    private IServiceItemService serviceItemService;

    /**
     * 查询服务项列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:serviceitem:list')")
    @GetMapping("/list")
    public TableDataInfo list(ServiceItem serviceItem)
    {
        startPage();
        List<ServiceItem> list = serviceItemService.selectServiceItemList(serviceItem);
        return getDataTable(list);
    }

    /**
     * 导出服务项列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:serviceitem:export')")
    @Log(title = "服务项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ServiceItem serviceItem)
    {
        List<ServiceItem> list = serviceItemService.selectServiceItemList(serviceItem);
        ExcelUtil<ServiceItem> util = new ExcelUtil<ServiceItem>(ServiceItem.class);
        util.exportExcel(response, list, "服务项数据");
    }

    /**
     * 获取服务项详细信息
     */
    @PreAuthorize("@ss.hasPermi('appointment:serviceitem:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(serviceItemService.selectServiceItemById(id));
    }

    /**
     * 获取审核人信息
     */
    @PreAuthorize("@ss.hasPermi('appointment:serviceitem:query')")
    @GetMapping(value = "/auditInfo/{id}")
    public AjaxResult getAuditInfo(@PathVariable Long id)
    {
        return success(serviceItemService.getAuditInfo(id));
    }

    /**
     * 新增服务项
     */
    @PreAuthorize("@ss.hasPermi('appointment:serviceitem:add')")
    @Log(title = "服务项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Valid ServiceItemVO serviceItem)
    {
        return toAjax(serviceItemService.insertServiceItem(serviceItem));
    }

    /**
     * 修改服务项
     */
    @PreAuthorize("@ss.hasPermi('appointment:serviceitem:edit')")
    @Log(title = "服务项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @Valid ServiceItemVO serviceItem)
    {
        return toAjax(serviceItemService.updateServiceItem(serviceItem));
    }

    /**
     * 删除服务项
     */
    @PreAuthorize("@ss.hasPermi('appointment:serviceitem:salestatus')")
    @Log(title = "服务项", businessType = BusinessType.UPDATE)
    @PatchMapping("/SaleStatus/{id}")
    public AjaxResult updateSaleStatus(@PathVariable Long id)
    {
        return toAjax(serviceItemService.updateSaleStatus(id));
    }
}
