package cn.wolfcode.business.appointment.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 结算单对象 bus_statement
 * 
 * @author wolfcode
 * @date 2024-01-22
 */
public class StatementVO
{
    private static final long serialVersionUID = 1L;

    //消费状态
    public static final Long CONSUMER_CENTER = 0L;
    public static final Long HAVE_PAID = 1L;


    /** $column.columnComment */
    private Long id;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    @NotEmpty(message = "客户姓名不能为空")
    private String customerName;

    /** 客户联系方式 */
    @NotEmpty(message = "客户联系方式不能为空")
    private String customerPhone;

    /** 实际到店时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "实际到店时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "实际到店时间不能为空")
    private Date actualArrivalTime;

    /** 车牌号码 */
    @NotEmpty(message = "车牌号码不能为空")
    private String licensePlate;

    /** 汽车类型 */
    @Excel(name = "汽车类型")
    @NotEmpty(message = "汽车类型不能为空")
    private String carSeries;

    /** 服务类型 */
    @Excel(name = "服务类型")
    @NotNull(message = "服务类型不能为空")
    private Long serviceType;

    /** 备注信息 */
    @Excel(name = "备注信息")
    private String info;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCustomerName(String customerName) 
    {
        this.customerName = customerName;
    }

    public String getCustomerName() 
    {
        return customerName;
    }
    public void setCustomerPhone(String customerPhone) 
    {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPhone() 
    {
        return customerPhone;
    }
    public void setActualArrivalTime(Date actualArrivalTime) 
    {
        this.actualArrivalTime = actualArrivalTime;
    }

    public Date getActualArrivalTime() 
    {
        return actualArrivalTime;
    }
    public void setLicensePlate(String licensePlate) 
    {
        this.licensePlate = licensePlate;
    }

    public String getLicensePlate() 
    {
        return licensePlate;
    }
    public void setCarSeries(String carSeries) 
    {
        this.carSeries = carSeries;
    }

    public String getCarSeries() 
    {
        return carSeries;
    }
    public void setServiceType(Long serviceType) 
    {
        this.serviceType = serviceType;
    }

    public Long getServiceType() 
    {
        return serviceType;
    }
    public void setInfo(String info)
    {
        this.info = info;
    }

    public String getInfo() 
    {
        return info;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerName", getCustomerName())
            .append("customerPhone", getCustomerPhone())
            .append("actualArrivalTime", getActualArrivalTime())
            .append("licensePlate", getLicensePlate())
            .append("carSeries", getCarSeries())
            .append("serviceType", getServiceType())
            .append("info", getInfo())
            .toString();
    }
}
