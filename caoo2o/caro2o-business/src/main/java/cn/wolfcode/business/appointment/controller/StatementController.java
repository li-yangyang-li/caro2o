package cn.wolfcode.business.appointment.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import cn.wolfcode.business.appointment.domain.StatementItem;
import cn.wolfcode.business.appointment.mapper.StatementItemMapper;
import cn.wolfcode.business.appointment.vo.StatementVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.appointment.domain.Statement;
import cn.wolfcode.business.appointment.service.IStatementService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 结算单Controller
 * 
 * @author wolfcode
 * @date 2024-01-22
 */
@RestController
@RequestMapping("/appointment/statement")
public class StatementController extends BaseController
{
    @Autowired
    private IStatementService statementService;


    /**
     * 查询结算单列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:statement:list')")
    @GetMapping("/list")
    public TableDataInfo list(Statement statement)
    {
        startPage();
        List<Statement> list = statementService.selectStatementList(statement);
        return getDataTable(list);
    }
    /**
     * 查询结算单明细
     *
     */
    @PreAuthorize("@ss.hasPermi('appointment:statementitem:list')")
    @GetMapping("/item/{id}")
    public AjaxResult itemList(@PathVariable Long id)
    {
        List<StatementItem> list = statementService.selectStatementItemList(id);
        return AjaxResult.success(list);
    }

    /**
     * 导出结算单列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:statement:export')")
    @Log(title = "结算单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Statement statement)
    {
        List<Statement> list = statementService.selectStatementList(statement);
        ExcelUtil<Statement> util = new ExcelUtil<Statement>(Statement.class);
        util.exportExcel(response, list, "结算单数据");
    }

    /**
     * 获取结算单详细信息
     */
    @PreAuthorize("@ss.hasPermi('appointment:statement:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(statementService.selectStatementById(id));
    }

    /**
     * 新增结算单
     */
    @PreAuthorize("@ss.hasPermi('appointment:statement:add')")
    @Log(title = "结算单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Valid StatementVO statement)
    {
        return toAjax(statementService.insertStatement(statement));
    }

    /**
     * 修改结算单
     */
    @PreAuthorize("@ss.hasPermi('appointment:statement:edit')")
    @Log(title = "结算单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StatementVO statement)
    {
        return toAjax(statementService.updateStatement(statement));
    }

    /**
     * 删除结算单
     */
    @PreAuthorize("@ss.hasPermi('appointment:statement:remove')")
    @Log(title = "结算单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(statementService.deleteStatementByIds(ids));
    }

    /**
     * 保存明细
     */
    @PreAuthorize("@ss.hasPermi('appointment:statement:saveItems')")
    @Log(title = "明细", businessType = BusinessType.INSERT)
    @PostMapping("/saveItems")
    public AjaxResult saveItems(@RequestBody Statement statement)
    {
        statementService.saveItems(statement);
        return AjaxResult.success();
    }
    @PreAuthorize("@ss.hasPermi('appointment:statement:saveItems')")
    @Log(title = "明细", businessType = BusinessType.UPDATE)
    @GetMapping("/pay/{id}")
    public AjaxResult pay(@PathVariable Long id)
    {
        statementService.updateStatementPay(id);
        return AjaxResult.success();
    }
    /**
     * 生成结算单
     * @param id
     * @return
     */
    @Log(title = "养修信息预约", businessType = BusinessType.UPDATE)
    @PostMapping("/createStatement/{id}")
    public AjaxResult createStatement(@PathVariable Long id)
    {
        Statement statement=  statementService.createStatement(id);
        return AjaxResult.success(statement);
    }

}
