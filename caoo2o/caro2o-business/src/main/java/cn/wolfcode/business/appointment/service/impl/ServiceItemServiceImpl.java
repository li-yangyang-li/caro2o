package cn.wolfcode.business.appointment.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wolfcode.business.appointment.vo.ServiceItemVO;
import cn.wolfcode.common.core.domain.entity.SysUser;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.bean.BeanUtils;
import cn.wolfcode.flow.bpmninfo.domain.BpmnInfo;
import cn.wolfcode.flow.bpmninfo.service.IBpmnInfoService;
import cn.wolfcode.flow.bpmninfo.service.IProcessService;
import cn.wolfcode.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.appointment.mapper.ServiceItemMapper;
import cn.wolfcode.business.appointment.domain.ServiceItem;
import cn.wolfcode.business.appointment.service.IServiceItemService;
import org.springframework.util.Assert;

import static cn.wolfcode.business.appointment.domain.ServiceItem.*;
import static cn.wolfcode.flow.bpmninfo.domain.BpmnInfo.BPMN_TYPE_AUDIT;

/**
 * 服务项Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-01-21
 */
@Service
public class ServiceItemServiceImpl implements IServiceItemService 
{
    @Autowired
    private ServiceItemMapper serviceItemMapper;

    /**
     * 查询服务项
     * 
     * @param id 服务项主键
     * @return 服务项
     */
    @Override
    public ServiceItem selectServiceItemById(Long id)
    {
        return serviceItemMapper.selectServiceItemById(id);
    }

    /**
     * 查询服务项列表
     * 
     * @param serviceItem 服务项
     * @return 服务项
     */
    @Override
    public List<ServiceItem> selectServiceItemList(ServiceItem serviceItem)
    {
        return serviceItemMapper.selectServiceItemList(serviceItem);
    }

    /**
     * 新增服务项
     * 
     * @param serviceItem 服务项
     * @return 结果
     */
    @Override
    public int insertServiceItem(ServiceItemVO serviceItem)
    {
        //将vo对象转换为domain对象
        ServiceItem serviceItemDest = new ServiceItem();
        BeanUtils.copyBeanProp(serviceItemDest,serviceItem);
        //根据是否是套餐为审核状态设置默认值
        if (serviceItemDest.getCarPackage() == CAR_PACKAGE_NO){
            //不是套餐设置审核状态为无需审核
            serviceItemDest.setAuditStatus(AUDIT_STATUS_NO);
        }else {
            //是套餐设置审核状态为初始化
            serviceItemDest.setAuditStatus(AUDIT_STATUS_INIT);
        }

        serviceItemDest.setCreateTime(DateUtils.getNowDate());
        return serviceItemMapper.insertServiceItem(serviceItemDest);
    }

    /**
     * 修改服务项
     * 
     * @param serviceItem 服务项
     * @return 结果
     */
    @Override
    public int updateServiceItem(ServiceItemVO serviceItem)
    {
        Assert.notNull(serviceItem.getId(),"参数错误");
        //通过id查询当前的服务项
        ServiceItem serviceItem1 = serviceItemMapper.selectServiceItemById(serviceItem.getId());
        //状态检查
        Assert.notNull(serviceItem1,"当前服务项不存在");
        //只有当审核状态为未上架和不子在审核中,才可以修改
        Assert.state(serviceItem1.getSaleStatus() == SALE_STATUS_NO
                && serviceItem1.getAuditStatus() != AUDIT_STATUS_AUDIT,"当前状态不允许修改");
        //设置审核状态
        //将vo对象转换为domain对象
        ServiceItem serviceItemDest = new ServiceItem();
        BeanUtils.copyBeanProp(serviceItemDest,serviceItem);
        if (serviceItem.getCarPackage() == CAR_PACKAGE_NO){
            //不是套餐设置审核状态为无需审核
            serviceItemDest.setAuditStatus(AUDIT_STATUS_NO);
        }else {
            //是套餐设置审核状态为初始化
            serviceItemDest.setAuditStatus(AUDIT_STATUS_INIT);
        }
        return serviceItemMapper.updateServiceItem(serviceItemDest);
    }

    /**
     * 批量删除服务项
     * 
     * @param ids 需要删除的服务项主键
     * @return 结果
     */
    @Override
    public int deleteServiceItemByIds(Long[] ids)
    {
        return serviceItemMapper.deleteServiceItemByIds(ids);
    }

    /**
     * 删除服务项信息
     * 
     * @param id 服务项主键
     * @return 结果
     */
    @Override
    public int deleteServiceItemById(Long id)
    {
        return serviceItemMapper.deleteServiceItemById(id);
    }

    @Override
    public int updateSaleStatus(Long id) {
        //状态判断
        Assert.notNull(id,"参数错误");
        ServiceItem serviceItem = serviceItemMapper.selectServiceItemById(id);
        Assert.notNull(serviceItem,"当前访问项不存在");
        Integer status = SALE_STATUS_NO;
        if (serviceItem.getSaleStatus() == SALE_STATUS_NO){
            //上架
            //状态为无需审核或审核通过才能上架
            Assert.state(serviceItem.getAuditStatus() ==AUDIT_STATUS_NO || serviceItem.getAuditStatus() == AUDIT_STATUS_PASSED,"当前状态不允许上架");
            status = SALE_STATUS_YES;
        }

        return serviceItemMapper.updateSaleStatus(id,status);
    }

    @Autowired
    private ISysUserService service;
    @Autowired
    private IProcessService processService;
    @Autowired
    private IBpmnInfoService bpmnInfoService;
    @Override
    public Map<String, Object> getAuditInfo(Long id) {
        HashMap<String, Object> auditInfo = new HashMap<>();
        //查询店长
        List<SysUser> shopOwners = service.getUserByRoleKey("shopOwner");
        auditInfo.put("shopOwners",shopOwners);
        //获取限定折扣价
        //如果折扣价>3000,查查询财务
        //查询最新的bpmnInfo
        BpmnInfo bpmnInfo = bpmnInfoService.getLastBpmnInfo(BPMN_TYPE_AUDIT);
        BigDecimal limitDiscountPrice = processService.getlimitDiscountPriceByKey(bpmnInfo.getProcessDefinitionKey(),bpmnInfo.getVersion());
        //根据服务项id查询服务项
        Assert.notNull(id,"参数错误");
        ServiceItem serviceItem = serviceItemMapper.selectServiceItemById(id);
        Assert.notNull(serviceItem,"当前服务项不存在");
        if (serviceItem.getDiscountPrice().compareTo(limitDiscountPrice) > 0){
            //查询财务
            List<SysUser> financials = service.getUserByRoleKey("financial");
            auditInfo.put("financials",financials);
        }
        auditInfo.put("limitDiscountPrice",limitDiscountPrice);

        return auditInfo;
    }

    @Override
    public int updateStatus(Long id, Integer auditStatusAudit) {
        return serviceItemMapper.updateStatus(id,auditStatusAudit);
    }


}
