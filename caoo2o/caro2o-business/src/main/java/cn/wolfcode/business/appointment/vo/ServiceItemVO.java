package cn.wolfcode.business.appointment.vo;

import cn.wolfcode.common.annotation.Excel;
import cn.wolfcode.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 服务项对象 bus_service_item
 * 
 * @author wolfcode
 * @date 2024-01-21
 */
public class ServiceItemVO
{
    private static final long serialVersionUID = 1L;


    /** $column.columnComment */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    @NotEmpty(message = "姓名不能为空")
    private String name;

    /** 原价 */
    @Excel(name = "原价")
    @NotNull(message = "原价不能为空")
    private BigDecimal originalPrice;

    /** 折扣价 */
    @Excel(name = "折扣价")
    @NotNull(message = "折扣价不能为空")
    private BigDecimal discountPrice;

    /** 是否套餐 */
    @Excel(name = "是否套餐")
    @NotNull(message = "套餐类型不能为空")
    private Integer carPackage;

    /** 备注信息 */
    @Excel(name = "备注信息")
    @NotEmpty(message = "备注信息不能为空")
    private String info;

    /** 服务分类 */
    @Excel(name = "服务分类")
    @NotNull(message = "服务分类不能为空")
    private Integer serviceCatalog;


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setOriginalPrice(BigDecimal originalPrice) 
    {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getOriginalPrice() 
    {
        return originalPrice;
    }
    public void setDiscountPrice(BigDecimal discountPrice) 
    {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getDiscountPrice() 
    {
        return discountPrice;
    }
    public void setCarPackage(Integer carPackage) 
    {
        this.carPackage = carPackage;
    }

    public Integer getCarPackage() 
    {
        return carPackage;
    }
    public void setInfo(String info) 
    {
        this.info = info;
    }

    public String getInfo() 
    {
        return info;
    }
    public void setServiceCatalog(Integer serviceCatalog) 
    {
        this.serviceCatalog = serviceCatalog;
    }

    public Integer getServiceCatalog() 
    {
        return serviceCatalog;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("originalPrice", getOriginalPrice())
            .append("discountPrice", getDiscountPrice())
            .append("carPackage", getCarPackage())
            .append("info", getInfo())
            .append("serviceCatalog", getServiceCatalog())
            .toString();
    }
}
