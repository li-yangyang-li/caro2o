package cn.wolfcode.business.appointment.service.impl;

import java.math.BigDecimal;
import java.util.List;

import cn.wolfcode.business.appointment.domain.Appointment;
import cn.wolfcode.business.appointment.domain.ServiceItem;
import cn.wolfcode.business.appointment.domain.StatementItem;
import cn.wolfcode.business.appointment.mapper.AppointmentMapper;
import cn.wolfcode.business.appointment.mapper.StatementItemMapper;
import cn.wolfcode.business.appointment.service.IAppointmentService;
import cn.wolfcode.business.appointment.service.IServiceItemService;
import cn.wolfcode.business.appointment.vo.AppointmentVO;
import cn.wolfcode.business.appointment.vo.StatementVO;
import cn.wolfcode.common.enums.AppointmentStatusEnum;
import cn.wolfcode.common.utils.DateUtils;
import cn.wolfcode.common.utils.SecurityUtils;
import cn.wolfcode.common.utils.bean.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.wolfcode.business.appointment.mapper.StatementMapper;
import cn.wolfcode.business.appointment.domain.Statement;
import cn.wolfcode.business.appointment.service.IStatementService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import static cn.wolfcode.common.enums.AppointmentStatusEnum.*;
import static cn.wolfcode.common.enums.AppointmentStatusEnum.ARRIVED;

/**
 * 结算单Service业务层处理
 * 
 * @author wolfcode
 * @date 2024-01-22
 */
@Service
public class StatementServiceImpl implements IStatementService 
{
    @Autowired
    private StatementMapper statementMapper;

    @Autowired
    private IServiceItemService statementService;

    @Autowired
    private StatementItemMapper statementItemMapper;

    /**
     * 查询结算单
     * 
     * @param id 结算单主键
     * @return 结算单
     */
    @Override
    public Statement selectStatementById(Long id)
    {
        return statementMapper.selectStatementById(id);
    }

    /**
     * 查询结算单列表
     * 
     * @param statement 结算单
     * @return 结算单
     */
    @Override
    public List<Statement> selectStatementList(Statement statement)
    {
        return statementMapper.selectStatementList(statement);
    }

    /**
     * 新增结算单
     * 
     * @param statementvo 结算单
     * @return 结果
     */
    @Override
    public int insertStatement(StatementVO statementvo)
    {

        //将vo对象转换为domain对象
        Statement statement = new Statement();
        BeanUtils.copyBeanProp(statement,statementvo);
        statement.setCreateTime(DateUtils.getNowDate());
        return statementMapper.insertStatement(statement);
    }

    /**
     * 修改结算单
     * 
     * @param statementvo 结算单
     * @return 结果
     */
    @Override
    public int updateStatement(StatementVO statementvo)
    {
        //判断当前id是否为空
        Assert.notNull(statementvo.getId(),"参数错误");
        //判断当前结算单是否可以修改
        Statement statementById = statementMapper.selectStatementById(statementvo.getId());
        Assert.notNull(statementById.getStatus() == StatementVO.CONSUMER_CENTER,"该状态不能修改");

        //将vo对象转换为domain对象
        Statement statement = new Statement();
        BeanUtils.copyBeanProp(statement,statementvo);
        statement.setCreateTime(DateUtils.getNowDate());
        return statementMapper.updateStatement(statement);
    }

    /**
     * 批量删除结算单
     * 
     * @param ids 需要删除的结算单主键
     * @return 结果
     */
    @Override
    public int deleteStatementByIds(Long[] ids)
    {
        //判断当前id是为null
        Assert.notNull(ids,"参数错误");
        for (int i = 0; i < ids.length; i++) {
            Statement statement = statementMapper.selectStatementById(ids[i]);
            Assert.state(statement.getStatus() != StatementVO.HAVE_PAID,"该状态不能删除");
        }
        return statementMapper.deleteStatementByIds(ids);
    }

    /**
     * 删除结算单信息
     * 
     * @param id 结算单主键
     * @return 结果
     */
    @Override
    public int deleteStatementById(Long id)
    {
        Assert.notNull(id,"参数错误");
        Statement statement = statementMapper.selectStatementById(id);
        Assert.state(statement.getStatus() != StatementVO.HAVE_PAID,"该状态不能删除");
        return statementMapper.deleteStatementById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveItems(Statement statement) {
        //先检查明细是否存在
        Assert.notEmpty(statement.getItems(),"请选择消费的服务项");
        //检查优惠金额是否大于总消费金额
        List<StatementItem> items = statement.getItems();
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalQuantity = BigDecimal.ZERO;
        //先删除用来的服务项
        statementItemMapper.deleteByStatementId(statement.getId());
        //保存明细
        for (StatementItem item : items) {
            //查询真实的服务项的价格
            Assert.notNull(item.getItemId(),"参数错误");
            ServiceItem serviceItem = statementService.selectServiceItemById(item.getItemId());
            Assert.notNull(serviceItem,"服务项不存在");
            BigDecimal amount = serviceItem.getDiscountPrice().multiply(item.getItemQuantity());
            //统计总消费金额和总数量
            totalAmount = totalAmount.add(amount);
            totalQuantity = totalQuantity.add(item.getItemQuantity());
        }
        //a1.compareTo(a2)
        //返回为正数表示a1>a2, 返回为负数表示a1<a2, 返回为0表示a1==a2。
        Assert.state(totalAmount.compareTo(statement.getDiscountAmount()) >= 0,"优惠金额不能大于总消费金额");

        int rows = statementItemMapper.insertStatementItem(items);
        Assert.state(rows>0,"保存明细失败");
        //更新结算单的消费金额和总数量,折扣金额
        Statement st = new Statement();
        st.setDiscountAmount(statement.getDiscountAmount());
        st.setId(statement.getId());
        st.setTotalAmount(totalAmount);
        st.setTotalQuantity(totalQuantity);
        int rowss = statementMapper.updateStatement(st);
        Assert.state(rowss>0,"结算单保存失败");

    }

    @Override
    public List<StatementItem> selectStatementItemList(Long id) {

        return statementItemMapper.selectByStatementId(id);
    }

    @Autowired
    private IAppointmentService service;
    @Override
    public void updateStatementPay(Long id) {
        Assert.notNull(id,"参数错误");
        //先通过id查询结算单,状态为消费中才能修改
        Statement statement = statementMapper.selectStatementById(id);
        Assert.state(statement.getStatus() == StatementVO.CONSUMER_CENTER,"当前状态不支持支付");
        //1. 更新结算单的状态为已支付
        //更新支付时间和支付人
        statement.setPayeeId(SecurityUtils.getUserId());
        statement.setPayTime(DateUtils.getNowDate());
        statement.setStatus(StatementVO.HAVE_PAID);
        statementMapper.updateStatement(statement);
        //2. 如果是预约客户,更新业务状态为已支付
        if (statement.getAppointmentId() != null){
            service.updateAppointmentByStatus(statement.getAppointmentId());
        }
    }
    @Autowired
    private AppointmentMapper appointmentMapper;
    @Override
    public Statement createStatement(Long id) {
        Assert.notNull(id,"参数错误");
        //通过id查询预约单,判断当前预约单是否可以生成结算单
        Appointment appointment = appointmentMapper.selectAppointmentById(id);
        Assert.notNull(appointment,"预约单不存在");

        Assert.state(appointment.getStatus() == ARRIVED.ordinal()|| appointment.getStatus()== SETTLED.ordinal() ||appointment.getStatus()==PAID.ordinal(),"该状态不能生成结算单");

        Statement statement = null;

        if (appointment.getStatus() == ARRIVED.ordinal()){
            //生成结算单
            statement = getStatement(appointment);
            //保存结算单
            statementMapper.insertStatement(statement);
            //并将当前预约单的状态改为生成结算单
            appointmentMapper.updateAppointmentByStatus(appointment.getId(), SETTLED.ordinal());
        }else {
            //通过预约单id查询结算单
            statement = statementMapper.selectStatementByAppointmentId(id);
        }
        return statement;
    }

    private static Statement getStatement(Appointment appointment) {
        Statement statement;
        statement = new Statement();
        BeanUtils.copyBeanProp(statement, appointment);
        statement.setAppointmentId(appointment.getId());
        statement.setStatus(StatementVO.CONSUMER_CENTER);
        return statement;
    }
}
