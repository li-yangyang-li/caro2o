package cn.wolfcode.business.appointment.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import cn.wolfcode.business.appointment.domain.Statement;
import cn.wolfcode.business.appointment.vo.AppointmentVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.wolfcode.common.annotation.Log;
import cn.wolfcode.common.core.controller.BaseController;
import cn.wolfcode.common.core.domain.AjaxResult;
import cn.wolfcode.common.enums.BusinessType;
import cn.wolfcode.business.appointment.domain.Appointment;
import cn.wolfcode.business.appointment.service.IAppointmentService;
import cn.wolfcode.common.utils.poi.ExcelUtil;
import cn.wolfcode.common.core.page.TableDataInfo;

/**
 * 养修信息预约Controller
 * 
 * @author wolfcode
 * @date 2024-01-18
 */
@RestController
@RequestMapping("/appointment/info")
public class AppointmentController extends BaseController
{
    @Autowired
    private IAppointmentService appointmentService;

    /**
     * 查询养修信息预约列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(Appointment appointment)
    {
        startPage();
        List<Appointment> list = appointmentService.selectAppointmentList(appointment);
        return getDataTable(list);
    }

    /**
     * 导出养修信息预约列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:info:export')")
    @Log(title = "养修信息预约", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Appointment appointment)
    {
        List<Appointment> list = appointmentService.selectAppointmentList(appointment);
        ExcelUtil<Appointment> util = new ExcelUtil<Appointment>(Appointment.class);
        util.exportExcel(response, list, "养修信息预约数据");
    }

    /**
     * 获取养修信息预约详细信息
     */
    @PreAuthorize("@ss.hasPermi('appointment:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(appointmentService.selectAppointmentById(id));
    }

    /**
     * 新增养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('appointment:info:add')")
    @Log(title = "养修信息预约", businessType = BusinessType.INSERT)
    @PostMapping
    //@Validated和@Valid :指定添加时需要对数据做校验
    public AjaxResult add(@RequestBody @Valid AppointmentVO appointment)
    {
        return toAjax(appointmentService.insertAppointment(appointment));
    }

    /**
     * 修改养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('appointment:info:edit')")
    @Log(title = "养修信息预约", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody @Valid AppointmentVO appointment)
    {
        return toAjax(appointmentService.updateAppointment(appointment));
    }

    /**
     * 删除养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('appointment:info:remove')")
    @Log(title = "养修信息预约", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(appointmentService.deleteAppointmentByIds(ids));
    }

    /**
     * 修改到店状态
     */
    @Log(title = "养修信息预约", businessType = BusinessType.UPDATE)
    @PatchMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(appointmentService.patchAppointmentById(id));
    }
    /**
     * 取消预约单
     */
    @Log(title = "养修信息预约", businessType = BusinessType.UPDATE)
    @PatchMapping("cancel/{id}")
    public AjaxResult cancel(@PathVariable Long id)
    {
        return toAjax(appointmentService.cancelAppointmentById(id));
    }


}
