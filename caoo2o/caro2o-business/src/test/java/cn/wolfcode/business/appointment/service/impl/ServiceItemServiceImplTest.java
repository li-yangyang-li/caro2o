package cn.wolfcode.business.appointment.service.impl;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author zslstart
 * @Date 2024-01-30 11:51
 */
@SpringBootTest(classes = ServiceItemServiceImpl.class)
class ServiceItemServiceImplTest {

    @Autowired
    private RepositoryService repositoryService;
    @Test
    public void get() {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("car_package_audit")
                .processDefinitionVersion(9)
                .singleResult();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinition.getId());
        Process process = bpmnModel.getProcess(processDefinition.getKey());
        Collection<FlowElement> flowElements = process.getFlowElements();
        for (FlowElement flowElement : flowElements) {
            System.out.println(flowElement);
        }
    }
}